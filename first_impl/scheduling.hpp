#ifndef SCHEDULING_H
#define SCHEDULING_H

#include <vector>
#include "process.hpp"

extern bool OUTPUT;

std::vector<process> fcfs(const std::vector<process> *process_list);
std::vector<process> sjf(const std::vector<process> *process_list);
std::vector<process> srtf(const std::vector<process> *process_list);
std::vector<process> rr(const std::vector<process> *process_list, const int quantum);
std::vector<process> ps_non(const std::vector<process> *process_list);
std::vector<process> ps_pre(const std::vector<process> *process_list);
std::vector<process> ls(const std::vector<process> *process_list);
#endif
