#include <iostream>
#include <vector>
#include <algorithm>
#include <chrono>
#include <random>
#include "process.hpp"
#include "scheduling.hpp"

//Park-Miller pseudo-random number generator for lottery-scheduling
std::minstd_rand number_gen(std::chrono::system_clock::now().time_since_epoch().count());

bool is_earlier(process x, process y)
{
  //if insertionTime is equal, the process with lower id is preferred
  if (x.getInsertionTime() == y.getInsertionTime())
    return false;
  return x.getInsertionTime() < y.getInsertionTime();
}

bool is_shorter(process x, process y)
{
  //if duartion and insertion are equal, order via id
  if (x.getDuration() == y.getDuration())
    {
      if (x.getInsertionTime() == y.getInsertionTime())
	{
	  return false;
	}
      else
	{
	  return x.getInsertionTime() < y.getInsertionTime();
	}
    }
  return x.getDuration() < y.getDuration();
}

bool has_higher_priority(process x, process y)
{
  //if priority and insertion are equal, order via id
  if (x.getPriority() == y.getPriority())
    {
      if (x.getInsertionTime() == y.getInsertionTime())
	{
	  return false;
	}
      else
	{
	  return x.getInsertionTime() < y.getInsertionTime();
	}
    }
  return x.getPriority() < y.getPriority();
}

bool is_earlier_and_shorter(process x, process y)
{
  //if insertion and duration are equal, order via id
  if (x.getInsertionTime() == y.getInsertionTime())
    {
      if (x.getDuration() == y.getDuration())
	{
	  return false;
	}
      else
	{
	  return x.getDuration() < y.getDuration();
	}
    }
  
  return x.getInsertionTime() < y.getInsertionTime();
}

bool is_earlier_and_higher_priority(process x, process y)
{
  //if insertion and priority are equal, order via id
  if (x.getInsertionTime() == y.getInsertionTime())
    {
      if (x.getPriority() == y.getPriority())
	{
	  return false;
	}
      else
	{
	  return x.getPriority() < y.getPriority();
	}
    }
  return x.getInsertionTime() < y.getInsertionTime();
}

void print_processes(std::string name, const std::vector<process> *process_list)
{
  std::cout<<"\n"<<name<<std::endl;
  for(process p : *process_list)
    {
      std::cout<<"id: "<<p.getId()<<", insert: "<<p.getInsertionTime()<<", remaining: "<<p.getRemainingTime()<<std::endl;
    }
}

std::vector<process> fcfs(const std::vector<process> *process_list)
{
  std::vector<process> fcfs_process_list = *process_list;
  std::sort(fcfs_process_list.begin(), fcfs_process_list.end(), is_earlier);
  
  if (OUTPUT)
    print_processes("First-come first-served", &fcfs_process_list);
  return fcfs_process_list;
}

std::vector<process> sjf(const std::vector<process> *process_list)
{
  std::vector<process> ordered_process_list = *process_list;
  std::sort(ordered_process_list.begin(), ordered_process_list.end(), is_earlier_and_shorter);
  
  int current_position=0, current_index=0;
  process *current_process, *prev_process;
  bool potential_processes_found = false;
  std::vector<process> compare_process_list;
  std::vector<process> sjf_process_list;
  
  while (!ordered_process_list.empty())
    {
      potential_processes_found = false;
      compare_process_list.clear();
      current_index=0;
      current_process = &ordered_process_list.front();
      prev_process=NULL;

      if (current_process->getInsertionTime() > current_position)
	current_position = current_process->getInsertionTime();
      
      while (!potential_processes_found)
      	{	  
      	  current_process = &ordered_process_list.at(current_index);
      	  if (prev_process != NULL)
      	    {
	      // when old processes are done and new processes are in the future, than current_process is next
      	      if (current_process->getInsertionTime() > prev_process->getInsertionTime() &&
		  current_process->getInsertionTime() > current_position)
      		{
		  potential_processes_found = true;
      		  break;
      		}
      	    }
	  //compare_process_list contains all possible processes concerning insertion 
      	  compare_process_list.push_back(*current_process);
	  
      	  if ((current_index+1) == (int)ordered_process_list.size())	    
	    potential_processes_found = true;	 	    
      	  current_index++;
	  prev_process = current_process;
       	}
       
      std::sort(compare_process_list.begin(), compare_process_list.end(), is_shorter);
      current_position += compare_process_list.front().getDuration();
      sjf_process_list.push_back(compare_process_list.front());

      //removing used processes from initial list
      for (int i=0; i<(int)ordered_process_list.size(); i++)
	{
	  if (ordered_process_list.at(i).getId() == compare_process_list.front().getId())
	    {	      		
	      ordered_process_list.erase(ordered_process_list.begin()+i);			      
	    }
	}  	  
    }

  if (OUTPUT)
    print_processes("Shortest Job First", &sjf_process_list);
  return sjf_process_list;
}


std::vector<process> srtf(const std::vector<process> *process_list)
{
  std::vector<process> ordered_process_list = *process_list;
  std::sort(ordered_process_list.begin(), ordered_process_list.end(), is_earlier_and_shorter);

  int process_start_position=0, current_position=0, current_index=0, index_iterator=0;
  int remaining_duration = 0, split_duration=0;
  process *current_process=NULL;
  bool found_shorter = false, found_next = false;;
  std::vector<process> compare_process_list;
  std::vector<process> srtf_process_list;

  while (!ordered_process_list.empty())
    {     
      if (found_shorter)	
	found_shorter = false;
      else
	current_index = 0;
      
      found_next = false;
      current_process = &ordered_process_list.at(current_index);
      
      if (current_process->getInsertionTime() > current_position)
	current_position = current_process->getInsertionTime();	

      process_start_position = current_position;
      index_iterator = (current_index + 1);

      // if list is empty or insertion of the next process is after the current processes is finished
      if (index_iterator >= (int)ordered_process_list.size() ||
	  (current_position + current_process->getRemainingTime()) < ordered_process_list.at(index_iterator).getInsertionTime())
	{
	  current_process->setRemainingTime(0);
	  compare_process_list.push_back(*current_process);	    
	  ordered_process_list.erase(ordered_process_list.begin()+current_index);
	}
      
      else
	{
	  if (ordered_process_list.at(index_iterator).getInsertionTime() <= current_position)
	    {	 
	      int shortest_process_index = -1;
	      while (index_iterator < (int)ordered_process_list.size() &&
		     ordered_process_list.at(index_iterator).getInsertionTime() <= current_position)
		{
		  if (current_process->getRemainingTime() > ordered_process_list.at(index_iterator).getRemainingTime())
		    {
		      shortest_process_index = index_iterator;
		    }
		  
		  index_iterator++;
		}
	      
	      if (shortest_process_index != -1)
		{
		  compare_process_list.push_back(ordered_process_list.at(shortest_process_index));
		  ordered_process_list.erase(ordered_process_list.begin() + shortest_process_index);
		  found_next = true;
		}
	    }
	  
	  if (!found_next)
	    {
	      //shearch for shorter until end of list or until the insertion of next process is after the current process is finished
	      while (index_iterator < (int)ordered_process_list.size() && !found_shorter &&
		     (current_position + ordered_process_list.at(current_index).getRemainingTime()) > ordered_process_list.at(index_iterator).getInsertionTime())
		{
		  //termination-time from current_process - insertion-time from next process
		  remaining_duration = (process_start_position + current_process->getRemainingTime()) - ordered_process_list.at(index_iterator).getInsertionTime();       
		  if (remaining_duration > ordered_process_list.at(index_iterator).getRemainingTime())
		    {		  
		      //split interrupted process
		      process split_process = ordered_process_list.at(current_index);
		      split_process.setRemainingTime(remaining_duration);

		      //add interrupted process to new vector and remove it from the ordered_process_list
		      split_duration = current_process->getRemainingTime();
		      split_process.setInsertionTime(current_position);
		      current_process->setRemainingTime(remaining_duration);
		      compare_process_list.push_back(*current_process);
		      ordered_process_list.insert(ordered_process_list.begin()+index_iterator+1, split_process);
		 
		      ordered_process_list.erase(ordered_process_list.begin());
		
		      found_shorter = true;
		      //index_iterator - 1, because one element was removed earlier
		      current_index = (index_iterator-1);
		    }
		  index_iterator++;
		}

	      //if no shorter process was found, continue with current process
	      if (!found_shorter)
		{
		  compare_process_list.push_back(*current_process);
		  current_position = (process_start_position + current_process->getRemainingTime());
		  ordered_process_list.erase(ordered_process_list.begin()+current_index);
		}
	    }
	}
    }

  if (OUTPUT)
    print_processes("Shortest Remaining Time First", &compare_process_list);
  return compare_process_list;
}




std::vector<process> rr(const std::vector<process> *process_list, const int quantum)
{
  std::vector<process> ordered_process_list = *process_list;
  std::sort(ordered_process_list.begin(), ordered_process_list.end(), is_earlier);

  int current_position = 0, index_iterator = 0;
  process *current_process;
  std::vector<process> rr_process_list;

  while (!ordered_process_list.empty())    
    {
      index_iterator = 0;
      current_process = &ordered_process_list.front();
      
      if (current_process->getInsertionTime() > current_position)
	current_position = current_process->getInsertionTime();
      
      if (current_process->getRemainingTime() > quantum)
	{
	  current_position += quantum;	  
	  process split_process = *current_process;
	  split_process.setInsertionTime(current_position);
	  split_process.setRemainingTime(split_process.getRemainingTime() - quantum);	 
	  rr_process_list.push_back(*current_process);

	  //search position in list, where process insertion is after current position
	  while (index_iterator < (int)ordered_process_list.size() &&
		 ordered_process_list.at(index_iterator).getInsertionTime() <= current_position)	    
	    index_iterator++;

	  //insert split process in the previously searched position
	  if (index_iterator > 0 || index_iterator <= ((int)ordered_process_list.size()-1)) 
	    ordered_process_list.insert(ordered_process_list.begin() + index_iterator, split_process);
	  else
	    ordered_process_list.push_back(split_process);
	  ordered_process_list.erase(ordered_process_list.begin());
	}
      
      else
	{
	  rr_process_list.push_back(*current_process);
	  current_position += quantum;
	  ordered_process_list.erase(ordered_process_list.begin());
	}
    }
  
  if (OUTPUT)
    print_processes("Round Robin", &rr_process_list);
  return rr_process_list;
}


std::vector<process> ps_non(const std::vector<process> *process_list)
{
  std::vector<process> ordered_process_list = *process_list;
  std::sort(ordered_process_list.begin(), ordered_process_list.end(), is_earlier_and_higher_priority);
  
  int current_position=0, current_index=0;
  process *current_process, *prev_process;
  bool potential_processes_found = false;
  std::vector<process> compare_process_list;
  std::vector<process> ps_non_process_list;
  
  while (!ordered_process_list.empty())
    {
      potential_processes_found = false;
      compare_process_list.clear();
      current_index=0;
      current_process = &ordered_process_list.front();
      prev_process=NULL;

      if (current_process->getInsertionTime() > current_position)
	current_position = current_process->getInsertionTime();
      
      while (!potential_processes_found)
      	{	  
      	  current_process = &ordered_process_list.at(current_index);
      	  if (prev_process != NULL)
      	    {
	      // when old processes are done and new processes are in the future, than current_process is next
      	      if (current_process->getInsertionTime() > prev_process->getInsertionTime() &&
		  current_process->getInsertionTime() > current_position)
      		{
		  potential_processes_found = true;
      		  break;
      		}
      	    }
	  
      	  compare_process_list.push_back(*current_process);	  
      	  if ((current_index+1) == (int)ordered_process_list.size())	    
	    potential_processes_found = true;	 	    
      	  current_index++;
	  prev_process = current_process;
       	}
       
      std::sort(compare_process_list.begin(), compare_process_list.end(), has_higher_priority);
      current_position += compare_process_list.front().getDuration();
      ps_non_process_list.push_back(compare_process_list.front());

      //removing used processes from initial list
      for (int i=0; i<(int)ordered_process_list.size(); i++)
	{
	  if (ordered_process_list.at(i).getId() == compare_process_list.front().getId())
	    {
		ordered_process_list.erase(ordered_process_list.begin()+i);		
		;break;
	    }
	}  	  
    }

  if (OUTPUT)
    print_processes("Shortest Job First", &ps_non_process_list);  
  return ps_non_process_list;
}



std::vector<process> ps_pre(const std::vector<process> *process_list)
{
  std::vector<process> ordered_process_list = *process_list;
  std::sort(ordered_process_list.begin(), ordered_process_list.end(), is_earlier_and_higher_priority);

  
  int process_start_position=0, current_position=0, current_index=0, index_iterator=0;
  int remaining_duration = 0, split_duration=0;
  process *current_process=NULL;
  bool found_shorter = false, found_next = false;;
  std::vector<process> compare_process_list;
  std::vector<process> ps_pre_process_list;
  
  while (!ordered_process_list.empty())
    {     
      if (found_shorter)	
	found_shorter = false;
      else
	current_index = 0;
      
      found_next = false;
      current_process = &ordered_process_list.at(current_index);
      if (current_process->getInsertionTime() > current_position)
	current_position = current_process->getInsertionTime();	
      process_start_position = current_position;
      index_iterator = (current_index + 1);

      //if end of list or insertion of next process is after current process is finished
      if (index_iterator >= (int)ordered_process_list.size() ||
	  (current_position + current_process->getRemainingTime()) < ordered_process_list.at(index_iterator).getInsertionTime())
	{
	  current_process->setRemainingTime(0);
	  compare_process_list.push_back(*current_process);	    

	  if ((int)ordered_process_list.size() > 1)
	    ordered_process_list.erase(ordered_process_list.begin()+current_index);
	  else
	    ordered_process_list.clear();
	}
      
      else
	{
	  if (ordered_process_list.at(index_iterator).getInsertionTime() <= current_position)
	    {	 
	      int highest_priority_process_index = -1;
	      while (index_iterator < (int)ordered_process_list.size() &&
		     ordered_process_list.at(index_iterator).getInsertionTime() <= current_position)
		{
		  if(current_process->getPriority() > ordered_process_list.at(index_iterator).getPriority())
		    {
		      highest_priority_process_index = index_iterator;
		    }
		  index_iterator++;
		}
	      
	      if (highest_priority_process_index != -1)
		{
		  compare_process_list.push_back(ordered_process_list.at(highest_priority_process_index));
		  ordered_process_list.erase(ordered_process_list.begin() + highest_priority_process_index);
		  found_next = true;
		}
	    }
	  
	  if (!found_next)
	    {
	      while (index_iterator < (int)ordered_process_list.size() && !found_shorter &&
		     (current_position + ordered_process_list.at(current_index).getRemainingTime()) > ordered_process_list.at(index_iterator).getInsertionTime())
		{
		  //termination-time from current_process - insertion-time from next process
		  remaining_duration = (process_start_position + current_process->getRemainingTime()) - ordered_process_list.at(index_iterator).getInsertionTime();       
		  if (current_process->getPriority() > ordered_process_list.at(index_iterator).getPriority())
		    {
		      //split interrupted process
		      process split_process = ordered_process_list.at(current_index);
		      split_process.setRemainingTime(remaining_duration);

		      //add interrupted process to new vector and remove it from the ordered_process_list
		      split_duration = current_process->getRemainingTime();
		      split_process.setInsertionTime(current_position);
		      current_process->setRemainingTime(remaining_duration);
		      compare_process_list.push_back(*current_process);
		      ordered_process_list.insert(ordered_process_list.begin()+index_iterator+1, split_process);
		 
		      ordered_process_list.erase(ordered_process_list.begin());
		
		      found_shorter = true;
		      //index_iterator - 1, because one element was removed earlier
		      current_index = (index_iterator-1);
		    }
		  index_iterator++;
		}

	      if (!found_shorter)
		{
		  compare_process_list.push_back(*current_process);
		  current_position = (process_start_position + current_process->getRemainingTime());
		  ordered_process_list.erase(ordered_process_list.begin()+current_index);

		}
	    }
	}
    }

  if (OUTPUT)
    print_processes("Priority Scheduling Preemtive", &compare_process_list);
  return compare_process_list;
}


//lottery-scheduling preemptive, without quantum
std::vector<process> ls(const std::vector<process> *process_list)
{
  std::vector<process> ordered_process_list = *process_list;
  std::sort(ordered_process_list.begin(), ordered_process_list.end(), is_earlier);

  std::vector<process> lottery_ticket_list;
  std::vector<process> lottery_process_list;

  int total_tickets = 0, ticket_counter = 0, winning_number = 0;
  int process_remaining_time = 0, current_position = 0, process_start_position = 0;
  process running_process, current_process, saved_process;

  while (!ordered_process_list.empty())
    {      
      if (lottery_ticket_list.empty())
	{
	  //add first process to the lottery list
	  lottery_ticket_list.push_back(ordered_process_list.front());
	  process_start_position = ordered_process_list.front().getInsertionTime();
	  current_position = process_start_position;
	  total_tickets += ordered_process_list.front().getTickets();
	  ordered_process_list.erase(ordered_process_list.begin());
	  running_process = lottery_ticket_list.front();

	  //add all processes which start at the same time as the first one
	  while (!ordered_process_list.empty() &&
		 ordered_process_list.front().getInsertionTime() == current_position)
	    {
	      total_tickets += ordered_process_list.front().getTickets();
	      lottery_ticket_list.push_back(ordered_process_list.front());
	      ordered_process_list.erase(ordered_process_list.begin());
	    }
	}
      
      else
	{
	  current_process = ordered_process_list.front();
	  ordered_process_list.erase(ordered_process_list.begin());
	  lottery_ticket_list.push_back(current_process);
	  total_tickets += current_process.getTickets();

	  //add all processes with the same insertion time
	  while(!ordered_process_list.empty() &&
		ordered_process_list.front().getInsertionTime() == current_process.getInsertionTime())
	    {
	      total_tickets += ordered_process_list.front().getTickets();
	      lottery_ticket_list.push_back(ordered_process_list.front());
	      ordered_process_list.erase(ordered_process_list.begin());

	    }
		
	  
	  process_remaining_time = current_position;
      
	  if (current_position < current_process.getInsertionTime())
	    current_position = current_process.getInsertionTime();
	  else
	    current_position += running_process.getRemainingTime();

	  //save and remove the process, which just ran
	  saved_process = running_process;
	  for (int i=0; i<(int)lottery_ticket_list.size(); i++)
	    {
	      if (lottery_ticket_list.at(i).getId() == running_process.getId())
		{
		  lottery_ticket_list.erase(lottery_ticket_list.begin() + i);
		  break;
		}
	    }
      
	  total_tickets -= saved_process.getTickets();

	  process_remaining_time = saved_process.getRemainingTime() - (current_position - process_remaining_time);

	  //if the process, which just ran, isn't done jet, put it back in the lottery list
	  if (process_remaining_time > 0)
	    {
	      saved_process.setRemainingTime(process_remaining_time);
	      lottery_ticket_list.push_back(saved_process);
	      total_tickets += saved_process.getTickets();
	    }   
	}

      //select a winner from the lottery list
      winning_number = ((number_gen() % total_tickets) + 1);
      ticket_counter = 0;
      for (process p : lottery_ticket_list)
	{
	  ticket_counter += p.getTickets();
	  if (ticket_counter >= winning_number)
	    {	      
	      running_process = p;
	      break;
	    }
	}
      lottery_process_list.push_back(running_process);				     
    }

  //if no new processes get into the list, select a winner and let it run until it's finished
  while (!lottery_ticket_list.empty())
    {
      winning_number = ((number_gen() % total_tickets) + 1);
      ticket_counter = 0;

      for (process p : lottery_ticket_list)
  	{
  	  ticket_counter += p.getTickets();
  	  if (ticket_counter >= winning_number)
  	    {	      
  	      running_process = p;
  	      break;
  	    }
  	}
      
      lottery_process_list.push_back(running_process);
      total_tickets -= running_process.getTickets();
      
      for (int i=0; i<(int)lottery_ticket_list.size(); i++)
  	{
  	  if (lottery_ticket_list.at(i).getId() == running_process.getId())
  	    {	
  	      lottery_ticket_list.erase(lottery_ticket_list.begin() + i);
  	      break;
  	    }
  	}
    }

  print_processes("Lottery Scheduling", &lottery_process_list);
  return lottery_process_list;
}
