#include "process.hpp"
#include <iostream>

process::process()
{  
  id = 0;
  insertion_time = 0;
  duration = 0;
  remaining_time = 0;
  priority = 0;
  tickets = 0;
}

process::process(int id_)
{
  id = id_;
  insertion_time = 0;
  duration = 0;
  remaining_time = 0;
  priority = 0;    
  tickets = 0;
}

process::process(int id_, int insertion_time_, int duration_, int priority_ )
{
  id = id_;
  insertion_time = insertion_time_;
  duration = duration_;
  remaining_time = duration;
  priority = priority_;
  tickets = (10 - priority_) * 10;
}

void process::setRemainingTime(int remaining_time_)
{
  remaining_time = remaining_time_;
}

void process::setDuration(int duration_)
{
  duration = duration_;
}

void process::setInsertionTime(int insertion_time_)
{
  insertion_time = insertion_time_;
}

void process::setTickets(int tickets_)
{
  tickets = tickets_;
}

int process::getId()
{
  return id;
}

int process::getDuration()
{
  return duration;
}

int process::getRemainingTime()
{
  return remaining_time;
}

int process::getInsertionTime()
{
  return insertion_time;
}

int process::getPriority()
{
  return priority;
}

int process::getTickets()
{
  return tickets;
}

std::ostream& operator<<(std::ostream &str, const process &p)
{
  return str<<p.id;
}
