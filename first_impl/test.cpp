#include "test.hpp"
#include "scheduling.hpp"

#include <algorithm>
#include <string>

bool OUTPUT = true;

test_object::test_object(std::string description_)
{
  description = description_;
}

void test_object::set_description(std::string description_)
{
  description = description_;
}

void test_object::set_fcfs_calc(std::vector<process> fcfs_calc_)
{
  fcfs_calc = fcfs_calc_;
}

void test_object::set_fcfs_sol(std::vector<process> fcfs_sol_)
{
  fcfs_sol = fcfs_sol_;
}

void test_object::set_sjf_calc(std::vector<process> sjf_calc_)
{
  sjf_calc = sjf_calc_;
}

void test_object::set_sjf_sol(std::vector<process> sjf_sol_)
{
  sjf_sol = sjf_sol_;
}

void test_object::set_srtf_calc(std::vector<process> srtf_calc_)
{
  srtf_calc = srtf_calc_;
}

void test_object::set_srtf_sol(std::vector<process> srtf_sol_)
{
  srtf_sol = srtf_sol_;
}

void test_object::set_rr_calc(std::vector<process> rr_calc_)
{
  rr_calc = rr_calc_;
}

void test_object::set_rr_sol(std::vector<process> rr_sol_)
{
  rr_sol = rr_sol_;
}

void test_object::set_ps_non_calc(std::vector<process> ps_non_calc_)
{
  ps_non_calc = ps_non_calc_;
}

void test_object::set_ps_non_sol(std::vector<process> ps_non_sol_)
{
  ps_non_sol = ps_non_sol_;
}

void test_object::set_ps_pre_calc(std::vector<process> ps_pre_calc_)
{
  ps_pre_calc = ps_pre_calc_;
}

void test_object::set_ps_pre_sol(std::vector<process> ps_pre_sol_)
{
  ps_pre_sol = ps_pre_sol_;
}

std::string test_object::get_description()
{
  return description;
}

std::vector<process> test_object::get_fcfs_calc()
{
  return fcfs_calc;
}

std::vector<process> test_object::get_fcfs_sol()
{
  return fcfs_sol;
}

std::vector<process> test_object::get_sjf_calc()
{
  return sjf_calc;
}

std::vector<process> test_object::get_sjf_sol()
{
  return sjf_sol;
}

std::vector<process> test_object::get_srtf_calc()
{
  return srtf_calc;
}

std::vector<process> test_object::get_srtf_sol()
{
  return srtf_sol;
}

std::vector<process> test_object::get_rr_calc()
{
  return rr_calc;
}

std::vector<process> test_object::get_rr_sol()
{
  return rr_sol;
}

std::vector<process> test_object::get_ps_non_calc()
{
  return ps_non_calc;
}

std::vector<process> test_object::get_ps_non_sol()
{
  return ps_non_sol;
}

std::vector<process> test_object::get_ps_pre_calc()
{
  return ps_pre_calc;
}

std::vector<process> test_object::get_ps_pre_sol()
{
  return ps_pre_sol;
}

int test_object::test_length()
{
  int errors = 0;
  if (fcfs_calc.size() != fcfs_sol.size())
    {
      std::cout<<description<<" fcfs-lists have different size!"<<std::endl;
      errors++;;
    }

  if (sjf_calc.size() != sjf_sol.size())
    {
      std::cout<<description<<" sjf-lists have different size!"<<std::endl;
      errors++;;
    }

  if (srtf_calc.size() != srtf_sol.size())
    {
      std::cout<<description<<" srtf-lists have different size!"<<std::endl;
      errors++;;
    }

  if (rr_calc.size() != rr_sol.size())
    {
      std::cout<<description<<" rr-lists have different size!"<<std::endl;
      errors++;;
    }

  if (ps_non_calc.size() != ps_non_sol.size())
    {
      std::cout<<description<<" ps_non-lists have different size!"<<std::endl;
      errors++;;
    }

  if (ps_pre_calc.size() != ps_pre_sol.size())
    {
      std::cout<<description<<" ps_pre-lists have different size!"<<std::endl;
      errors++;;
    }
  return errors;
}

int test_object::test_elements()
{
  int errors = 0;
  for (int i=0; i<(int)fcfs_calc.size(); i++)
    {
      if (fcfs_calc.at(i).getId() != fcfs_sol.at(i).getId())
	{
	  std::cout<<description<<" fcfs at "<<i<<" wrong\nIs: "<<fcfs_calc.at(i)
		   <<"  Should: "<<fcfs_sol.at(i)<<std::endl;
	  errors++;	    
	}
    }

  for (int i=0; i<(int)sjf_calc.size(); i++)
    {
      if (sjf_calc.at(i).getId() != sjf_sol.at(i).getId())
	{
	  std::cout<<description<<" sjf at "<<i<<" wrong\nIs: "<<sjf_calc.at(i)
		   <<"  Should: "<<sjf_sol.at(i)<<std::endl;
	  errors++;	    
	}
    }

  for (int i=0; i<(int)srtf_calc.size(); i++)
    {
      if (srtf_calc.at(i).getId() != srtf_sol.at(i).getId())
	{
	  std::cout<<description<<" srtf at "<<i<<" wrong\nIs: "<<srtf_calc.at(i)
		   <<"  Should: "<<srtf_sol.at(i)<<std::endl;
	  errors++;	    
	}
    }  

  for (int i=0; i<(int)rr_calc.size(); i++)
    {      
      if (rr_calc.at(i).getId() != rr_sol.at(i).getId())
	{
	  std::cout<<description<<" rr at "<<i<<" wrong\nIs: "<<rr_calc.at(i)
		   <<"  Should: "<<rr_sol.at(i)<<std::endl;
	  errors++;	    
	}
    }

  for (int i=0; i<(int)ps_non_calc.size(); i++)
    {
      if (ps_non_calc.at(i).getId() != ps_non_sol.at(i).getId())
	{
	  std::cout<<description<<" ps_non at "<<i<<" wrong\nIs: "<<ps_non_calc.at(i)
		   <<"  Should: "<<ps_non_sol.at(i)<<std::endl;
	  errors++;	    
	}
    }

  for (int i=0; i<(int)ps_pre_calc.size(); i++)
    {
      if (ps_pre_calc.at(i).getId() != ps_pre_sol.at(i).getId())
	{
	  std::cout<<description<<" ps_pre at "<<i<<" wrong\nIs: "<<ps_pre_calc.at(i)
		   <<"  Should: "<<ps_pre_sol.at(i)<<std::endl;
	  errors++;	    
	}
    }
  return errors;
}

test_object early_input_test("Ealry input");
test_object increasing_duration_test("Increasing duration");
test_object decreasing_duration_test("Decreaseing duration");
test_object increasing_insertion_test("Increasing insertion");
test_object decreasing_insertion_test("Decreasing Insertion");
test_object increasing_priority_test("Increasing Priority");
test_object decreasing_priority_test("Decreasing Priority");
test_object os_proseminar("Operating systems proseminar");

void create_test_input()
{
  std::vector<std::vector<process> > test_data;

  std::vector<process> early_input;
  std::vector<process> increasing_duration;
  std::vector<process> decreasing_duration;
  std::vector<process> increasing_insertion;
  std::vector<process> decreasing_insertion;
  std::vector<process> increasing_priority;
  std::vector<process> decreasing_priority;
  std::vector<process> os_proseminar_list;
  
  for(int i=1; i<=5; i++)
    {
      process new_early_process(i,1,2,5);
      early_input.push_back(new_early_process);

      process new_inc_duration__process(i,1,i,5);
      increasing_duration.push_back(new_inc_duration__process);

      process new_dec_duration_process(i,1,(10-i),5);
      decreasing_duration.push_back(new_dec_duration_process);

      process new_inc_insertion_process(i,i*2,5,5);
      increasing_insertion.push_back(new_inc_insertion_process);

      process new_dec_insertion_process(i,((10-i)*2),5,5);
      decreasing_insertion.push_back(new_dec_insertion_process);

      process new_inc_priority_process(i,1,5,i);
      increasing_priority.push_back(new_inc_priority_process);

      process new_dec_priority_process(i,1,5,(10-i));
      decreasing_priority.push_back(new_dec_priority_process);     
    }

  process p1(1,0,3,3);
  process p2(2,2,6,3);
  process p3(3,4,4,2);
  process p4(4,6,5,2);
  process p5(5,8,2,1);
  
  os_proseminar_list.push_back(p1);
  os_proseminar_list.push_back(p2);
  os_proseminar_list.push_back(p3);
  os_proseminar_list.push_back(p4);
  os_proseminar_list.push_back(p5);  
  
  test_data.push_back(early_input);
  test_data.push_back(increasing_duration);
  test_data.push_back(decreasing_duration);
  test_data.push_back(increasing_insertion);
  test_data.push_back(decreasing_insertion);
  test_data.push_back(increasing_priority);
  test_data.push_back(decreasing_priority);

  early_input_test.set_fcfs_calc(fcfs(&early_input));
  early_input_test.set_sjf_calc(sjf(&early_input));
  early_input_test.set_srtf_calc(srtf(&early_input));
  early_input_test.set_rr_calc(rr(&early_input,2));
  early_input_test.set_ps_non_calc(ps_non(&early_input));
  early_input_test.set_ps_pre_calc(ps_pre(&early_input));

  increasing_duration_test.set_fcfs_calc(fcfs(&increasing_duration));
  increasing_duration_test.set_sjf_calc(sjf(&increasing_duration));
  increasing_duration_test.set_srtf_calc(srtf(&increasing_duration));
  increasing_duration_test.set_rr_calc(rr(&increasing_duration,2));
  increasing_duration_test.set_ps_non_calc(ps_non(&increasing_duration));
  increasing_duration_test.set_ps_pre_calc(ps_pre(&increasing_duration));

  decreasing_duration_test.set_fcfs_calc(fcfs(&decreasing_duration));
  decreasing_duration_test.set_sjf_calc(sjf(&decreasing_duration));
  decreasing_duration_test.set_srtf_calc(srtf(&decreasing_duration));
  decreasing_duration_test.set_rr_calc(rr(&decreasing_duration,2));
  decreasing_duration_test.set_ps_non_calc(ps_non(&decreasing_duration));
  decreasing_duration_test.set_ps_pre_calc(ps_pre(&decreasing_duration));
  
  increasing_insertion_test.set_fcfs_calc(fcfs(&increasing_insertion));
  increasing_insertion_test.set_sjf_calc(sjf(&increasing_insertion));
  increasing_insertion_test.set_srtf_calc(srtf(&increasing_insertion));
  increasing_insertion_test.set_rr_calc(rr(&increasing_insertion,2));
  increasing_insertion_test.set_ps_non_calc(ps_non(&increasing_insertion));
  increasing_insertion_test.set_ps_pre_calc(ps_pre(&increasing_insertion));
  
  decreasing_insertion_test.set_fcfs_calc(fcfs(&decreasing_insertion));
  decreasing_insertion_test.set_sjf_calc(sjf(&decreasing_insertion));
  decreasing_insertion_test.set_srtf_calc(srtf(&decreasing_insertion));
  decreasing_insertion_test.set_rr_calc(rr(&decreasing_insertion,2));
  decreasing_insertion_test.set_ps_non_calc(ps_non(&decreasing_insertion));
  decreasing_insertion_test.set_ps_pre_calc(ps_pre(&decreasing_insertion));

  increasing_priority_test.set_fcfs_calc(fcfs(&increasing_priority));
  increasing_priority_test.set_sjf_calc(sjf(&increasing_priority));
  increasing_priority_test.set_srtf_calc(srtf(&increasing_priority));
  increasing_priority_test.set_rr_calc(rr(&increasing_priority,2));
  increasing_priority_test.set_ps_non_calc(ps_non(&increasing_priority));
  increasing_priority_test.set_ps_pre_calc(ps_pre(&increasing_priority));
  
  decreasing_priority_test.set_fcfs_calc(fcfs(&decreasing_priority));
  decreasing_priority_test.set_sjf_calc(sjf(&decreasing_priority));
  decreasing_priority_test.set_srtf_calc(srtf(&decreasing_priority));
  decreasing_priority_test.set_rr_calc(rr(&decreasing_priority,2));
  decreasing_priority_test.set_ps_non_calc(ps_non(&decreasing_priority));
  decreasing_priority_test.set_ps_pre_calc(ps_pre(&decreasing_priority));

  os_proseminar.set_fcfs_calc(fcfs(&os_proseminar_list));
  os_proseminar.set_sjf_calc(sjf(&os_proseminar_list));
  os_proseminar.set_srtf_calc(srtf(&os_proseminar_list));
  os_proseminar.set_rr_calc(rr(&os_proseminar_list,1));
  os_proseminar.set_ps_non_calc(ps_non(&os_proseminar_list));
  os_proseminar.set_ps_pre_calc(ps_pre(&os_proseminar_list));
}


void create_test_output()
{  
  std::vector<std::vector<process> > test_data;

  std::vector<process> early_input_fcfs;
  std::vector<process> early_input_sjf;
  std::vector<process> early_input_srtf;
  std::vector<process> early_input_rr;
  std::vector<process> early_input_ps_non;
  std::vector<process> early_input_ps_pre;

  std::vector<process> increasing_duration_fcfs;
  std::vector<process> increasing_duration_sjf;
  std::vector<process> increasing_duration_srtf;
  std::vector<process> increasing_duration_rr;
  std::vector<process> increasing_duration_ps_non;
  std::vector<process> increasing_duration_ps_pre;

  std::vector<process> decreasing_duration_fcfs;
  std::vector<process> decreasing_duration_sjf;
  std::vector<process> decreasing_duration_srtf;
  std::vector<process> decreasing_duration_rr;
  std::vector<process> decreasing_duration_ps_non;
  std::vector<process> decreasing_duration_ps_pre;

  std::vector<process> increasing_insertion_fcfs;
  std::vector<process> increasing_insertion_sjf;
  std::vector<process> increasing_insertion_srtf;
  std::vector<process> increasing_insertion_rr;
  std::vector<process> increasing_insertion_ps_non;
  std::vector<process> increasing_insertion_ps_pre;

  std::vector<process> decreasing_insertion_fcfs;
  std::vector<process> decreasing_insertion_sjf;
  std::vector<process> decreasing_insertion_srtf;
  std::vector<process> decreasing_insertion_rr;
  std::vector<process> decreasing_insertion_ps_non;
  std::vector<process> decreasing_insertion_ps_pre;

  std::vector<process> increasing_priority_fcfs;
  std::vector<process> increasing_priority_sjf;
  std::vector<process> increasing_priority_srtf;
  std::vector<process> increasing_priority_rr;
  std::vector<process> increasing_priority_ps_non;
  std::vector<process> increasing_priority_ps_pre;

  std::vector<process> decreasing_priority_fcfs;
  std::vector<process> decreasing_priority_sjf;
  std::vector<process> decreasing_priority_srtf;
  std::vector<process> decreasing_priority_rr;
  std::vector<process> decreasing_priority_ps_non;
  std::vector<process> decreasing_priority_ps_pre;       

  std::vector<process> os_proseminar_fcfs;
  std::vector<process> os_proseminar_sjf;
  std::vector<process> os_proseminar_srtf;
  std::vector<process> os_proseminar_rr;
  std::vector<process> os_proseminar_ps_non;
  std::vector<process> os_proseminar_ps_pre;       

  process p1(1);
  process p2(2);
  process p3(3);
  process p4(4);
  process p5(5);   

  for(int i=1; i<=5; i++)
    {
      process new_early_process(i,1,2,5);
      early_input_fcfs.push_back(new_early_process);
    }
  
  early_input_sjf = early_input_fcfs;
  early_input_srtf = early_input_fcfs;
  early_input_rr = early_input_fcfs;
  early_input_ps_non = early_input_fcfs;
  early_input_ps_pre = early_input_fcfs;

  increasing_duration_fcfs = early_input_fcfs;
  increasing_duration_sjf = early_input_fcfs;
  increasing_duration_srtf = early_input_fcfs;

  increasing_duration_rr.push_back(p1);
  increasing_duration_rr.push_back(p2);
  increasing_duration_rr.push_back(p3);
  increasing_duration_rr.push_back(p4);
  increasing_duration_rr.push_back(p5);
  increasing_duration_rr.push_back(p3);
  increasing_duration_rr.push_back(p4);
  increasing_duration_rr.push_back(p5);    
  increasing_duration_rr.push_back(p5);
  
  increasing_duration_ps_non = early_input_fcfs;
  increasing_duration_ps_pre = early_input_fcfs;

  decreasing_duration_fcfs = early_input_fcfs;
  decreasing_duration_sjf = early_input_fcfs;
  std::reverse(decreasing_duration_sjf.begin(), decreasing_duration_sjf.end());
  decreasing_duration_srtf = decreasing_duration_sjf;

  for (int i=0; i<3; i++)
    {
      decreasing_duration_rr.push_back(p1);
      decreasing_duration_rr.push_back(p2);
      decreasing_duration_rr.push_back(p3);
      decreasing_duration_rr.push_back(p4);
      decreasing_duration_rr.push_back(p5);
    }
  
  decreasing_duration_rr.push_back(p1);
  decreasing_duration_rr.push_back(p2);
  decreasing_duration_rr.push_back(p3);
  decreasing_duration_rr.push_back(p1);

  decreasing_duration_ps_non = early_input_fcfs;
  decreasing_duration_ps_pre = early_input_fcfs;
  
  increasing_insertion_fcfs = early_input_fcfs;
  increasing_insertion_sjf = early_input_fcfs;
  increasing_insertion_srtf = early_input_fcfs;

  increasing_insertion_rr.push_back(p1);
  increasing_insertion_rr.push_back(p2);
  increasing_insertion_rr.push_back(p1);
  increasing_insertion_rr.push_back(p3);
  increasing_insertion_rr.push_back(p2);
  increasing_insertion_rr.push_back(p4);
  increasing_insertion_rr.push_back(p1);
  increasing_insertion_rr.push_back(p5);
  increasing_insertion_rr.push_back(p3);
  increasing_insertion_rr.push_back(p2);
  increasing_insertion_rr.push_back(p4);
  increasing_insertion_rr.push_back(p5);
  increasing_insertion_rr.push_back(p3);
  increasing_insertion_rr.push_back(p4);
  increasing_insertion_rr.push_back(p5);
  
  increasing_insertion_ps_non = early_input_fcfs;
  increasing_insertion_ps_pre = early_input_fcfs;

  decreasing_insertion_fcfs = decreasing_duration_sjf;
  decreasing_insertion_sjf = decreasing_insertion_fcfs;
  decreasing_insertion_srtf = decreasing_duration_srtf;

  decreasing_insertion_rr.push_back(p5);
  decreasing_insertion_rr.push_back(p4);
  decreasing_insertion_rr.push_back(p5);
  decreasing_insertion_rr.push_back(p3);
  decreasing_insertion_rr.push_back(p4);
  decreasing_insertion_rr.push_back(p2);
  decreasing_insertion_rr.push_back(p5);
  decreasing_insertion_rr.push_back(p1);
  decreasing_insertion_rr.push_back(p3);
  decreasing_insertion_rr.push_back(p4);
  decreasing_insertion_rr.push_back(p2);
  decreasing_insertion_rr.push_back(p1);
  decreasing_insertion_rr.push_back(p3);
  decreasing_insertion_rr.push_back(p2);
  decreasing_insertion_rr.push_back(p1);
  
  decreasing_insertion_ps_non = decreasing_insertion_fcfs;
  decreasing_insertion_ps_pre = decreasing_insertion_fcfs;

  increasing_priority_fcfs = early_input_fcfs;
  increasing_priority_sjf = early_input_fcfs;
  increasing_priority_srtf = early_input_fcfs;

  for(int i=0; i<3; i++)
    {
      increasing_priority_rr.push_back(p1);
      increasing_priority_rr.push_back(p2);
      increasing_priority_rr.push_back(p3);
      increasing_priority_rr.push_back(p4);
      increasing_priority_rr.push_back(p5);
    }
  
  increasing_priority_ps_non = early_input_fcfs;
  increasing_priority_ps_pre = early_input_fcfs;
  
  decreasing_priority_fcfs = early_input_fcfs;
  decreasing_priority_sjf = early_input_fcfs;
  decreasing_priority_srtf = early_input_fcfs;
  decreasing_priority_rr = increasing_priority_rr;
  decreasing_priority_ps_non = decreasing_duration_sjf;
  decreasing_priority_ps_pre = decreasing_duration_sjf;

  os_proseminar_fcfs.push_back(p1);
  os_proseminar_fcfs.push_back(p2);
  os_proseminar_fcfs.push_back(p3);
  os_proseminar_fcfs.push_back(p4);
  os_proseminar_fcfs.push_back(p5);

  os_proseminar_sjf.push_back(p1);
  os_proseminar_sjf.push_back(p2);
  os_proseminar_sjf.push_back(p5);
  os_proseminar_sjf.push_back(p3);
  os_proseminar_sjf.push_back(p4);

  os_proseminar_srtf.push_back(p1);
  os_proseminar_srtf.push_back(p2);
  os_proseminar_srtf.push_back(p3);
  os_proseminar_srtf.push_back(p5);
  os_proseminar_srtf.push_back(p2);
  os_proseminar_srtf.push_back(p4);

  os_proseminar_rr.push_back(p1);
  os_proseminar_rr.push_back(p1);
  os_proseminar_rr.push_back(p2);
  os_proseminar_rr.push_back(p1);
  os_proseminar_rr.push_back(p2);
  os_proseminar_rr.push_back(p3);
  os_proseminar_rr.push_back(p2);
  os_proseminar_rr.push_back(p4);
  os_proseminar_rr.push_back(p3);
  os_proseminar_rr.push_back(p2);
  os_proseminar_rr.push_back(p5);
  os_proseminar_rr.push_back(p4);
  os_proseminar_rr.push_back(p3);
  os_proseminar_rr.push_back(p2);
  os_proseminar_rr.push_back(p5);
  os_proseminar_rr.push_back(p4);
  os_proseminar_rr.push_back(p3);
  os_proseminar_rr.push_back(p2);
  os_proseminar_rr.push_back(p4);
  os_proseminar_rr.push_back(p4);

  os_proseminar_ps_non.push_back(p1);
  os_proseminar_ps_non.push_back(p2);
  os_proseminar_ps_non.push_back(p5);
  os_proseminar_ps_non.push_back(p3);
  os_proseminar_ps_non.push_back(p4);

  os_proseminar_ps_pre.push_back(p1);
  os_proseminar_ps_pre.push_back(p2);
  os_proseminar_ps_pre.push_back(p3);
  os_proseminar_ps_pre.push_back(p5);
  os_proseminar_ps_pre.push_back(p4);
  os_proseminar_ps_pre.push_back(p2);

  
  early_input_test.set_fcfs_sol(early_input_fcfs);
  early_input_test.set_sjf_sol(early_input_sjf);
  early_input_test.set_srtf_sol(early_input_srtf);
  early_input_test.set_rr_sol(early_input_rr);
  early_input_test.set_ps_non_sol(early_input_ps_non);
  early_input_test.set_ps_pre_sol(early_input_ps_pre);

  increasing_duration_test.set_fcfs_sol(increasing_duration_fcfs);
  increasing_duration_test.set_sjf_sol(increasing_duration_sjf);
  increasing_duration_test.set_srtf_sol(increasing_duration_srtf);
  increasing_duration_test.set_rr_sol(increasing_duration_rr);
  increasing_duration_test.set_ps_non_sol(increasing_duration_ps_non);
  increasing_duration_test.set_ps_pre_sol(increasing_duration_ps_pre);

  decreasing_duration_test.set_fcfs_sol(decreasing_duration_fcfs);
  decreasing_duration_test.set_sjf_sol(decreasing_duration_sjf);
  decreasing_duration_test.set_srtf_sol(decreasing_duration_srtf);
  decreasing_duration_test.set_rr_sol(decreasing_duration_rr);
  decreasing_duration_test.set_ps_non_sol(decreasing_duration_ps_non);
  decreasing_duration_test.set_ps_pre_sol(decreasing_duration_ps_pre);

  increasing_insertion_test.set_fcfs_sol(increasing_insertion_fcfs);
  increasing_insertion_test.set_sjf_sol(increasing_insertion_sjf);
  increasing_insertion_test.set_srtf_sol(increasing_insertion_srtf);
  increasing_insertion_test.set_rr_sol(increasing_insertion_rr);
  increasing_insertion_test.set_ps_non_sol(increasing_insertion_ps_non);
  increasing_insertion_test.set_ps_pre_sol(increasing_insertion_ps_pre);

  decreasing_insertion_test.set_fcfs_sol(decreasing_insertion_fcfs);
  decreasing_insertion_test.set_sjf_sol(decreasing_insertion_sjf);
  decreasing_insertion_test.set_srtf_sol(decreasing_insertion_srtf);
  decreasing_insertion_test.set_rr_sol(decreasing_insertion_rr);
  decreasing_insertion_test.set_ps_non_sol(decreasing_insertion_ps_non);
  decreasing_insertion_test.set_ps_pre_sol(decreasing_insertion_ps_pre);

  increasing_priority_test.set_fcfs_sol(increasing_priority_fcfs);
  increasing_priority_test.set_sjf_sol(increasing_priority_sjf);
  increasing_priority_test.set_srtf_sol(increasing_priority_srtf);
  increasing_priority_test.set_rr_sol(increasing_priority_rr);
  increasing_priority_test.set_ps_non_sol(increasing_priority_ps_non);
  increasing_priority_test.set_ps_pre_sol(increasing_priority_ps_pre);

  decreasing_priority_test.set_fcfs_sol(decreasing_priority_fcfs);
  decreasing_priority_test.set_sjf_sol(decreasing_priority_sjf);
  decreasing_priority_test.set_srtf_sol(decreasing_priority_srtf);
  decreasing_priority_test.set_rr_sol(decreasing_priority_rr);
  decreasing_priority_test.set_ps_non_sol(decreasing_priority_ps_non);
  decreasing_priority_test.set_ps_pre_sol(decreasing_priority_ps_pre);

  os_proseminar.set_fcfs_sol(os_proseminar_fcfs);
  os_proseminar.set_sjf_sol(os_proseminar_sjf);
  os_proseminar.set_srtf_sol(os_proseminar_srtf);
  os_proseminar.set_rr_sol(os_proseminar_rr);
  os_proseminar.set_ps_non_sol(os_proseminar_ps_non);
  os_proseminar.set_ps_pre_sol(os_proseminar_ps_pre);  
}

void run_tests(bool output)
{
  if(!output)
    OUTPUT = false;
  create_test_input();
  create_test_output();

  int length_errors = 0, position_errors = 0;
  
  length_errors =
    early_input_test.test_length() +
    increasing_duration_test.test_length() +
    decreasing_duration_test.test_length() +
    increasing_insertion_test.test_length() +
    decreasing_insertion_test.test_length() +
    increasing_priority_test.test_length() +
    decreasing_priority_test.test_length() +
    os_proseminar.test_length();
  
  if(length_errors == 0)
    {
      position_errors =
	early_input_test.test_elements() +
	increasing_duration_test.test_elements() +
	decreasing_duration_test.test_elements() +
	increasing_insertion_test.test_elements() +
	decreasing_insertion_test.test_elements() +
	increasing_priority_test.test_elements() +
	decreasing_priority_test.test_elements() +
	os_proseminar.test_elements();
    }
  std::cout<<"\nLength errors: "<<length_errors<<std::endl;
  std::cout<<"Position errors: "<<position_errors<<"\n"<<std::endl;
}
