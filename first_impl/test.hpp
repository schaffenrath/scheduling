#ifndef TEST_H
#define TEST_H

#include <vector>
#include <string>
#include "process.hpp"

extern bool OUTPUT;

class test_object{
private:
  std::string description;
  std::vector<process> fcfs_calc;
  std::vector<process> fcfs_sol;
  std::vector<process> sjf_calc;
  std::vector<process> sjf_sol;
  std::vector<process> srtf_calc;
  std::vector<process> srtf_sol;
  std::vector<process> rr_calc;
  std::vector<process> rr_sol;
  std::vector<process> ps_non_calc;
  std::vector<process> ps_non_sol;
  std::vector<process> ps_pre_calc;
  std::vector<process> ps_pre_sol;

public:
  test_object(std::string description_);
  void set_description(std::string description_);
  void set_fcfs_calc(std::vector<process> fcfs_calc_);
  void set_fcfs_sol(std::vector<process> fcfs_sol_);
  void set_sjf_calc(std::vector<process> sjf_calc_);
  void set_sjf_sol(std::vector<process> sjf_sol_);
  void set_srtf_calc(std::vector<process> srtf_calc_);
  void set_srtf_sol(std::vector<process> srtf_sol_);
  void set_rr_calc(std::vector<process> rr_calc_);
  void set_rr_sol(std::vector<process> rr_sol_);
  void set_ps_non_calc(std::vector<process> ps_non_calc_);
  void set_ps_non_sol(std::vector<process> ps_non_sol_);
  void set_ps_pre_calc(std::vector<process> ps_pre_calc_);
  void set_ps_pre_sol(std::vector<process> ps_pre_sol_);
  std::string get_description();
  std::vector<process> get_fcfs_calc();
  std::vector<process> get_fcfs_sol();
  std::vector<process> get_sjf_calc();
  std::vector<process> get_sjf_sol();
  std::vector<process> get_srtf_calc();
  std::vector<process> get_srtf_sol();
  std::vector<process> get_rr_calc();
  std::vector<process> get_rr_sol();
  std::vector<process> get_ps_non_calc();
  std::vector<process> get_ps_non_sol();
  std::vector<process> get_ps_pre_calc();
  std::vector<process> get_ps_pre_sol();
  int test_length();
  int test_elements();
};

std::vector<std::vector<process> > test_input();
std::vector<std::vector<process> > test_output();
void run_tests(bool output);

#endif
