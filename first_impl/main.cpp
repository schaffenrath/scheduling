#include <iostream>
#include <vector>
#include <string>
#include <cstring>
#include <ctype.h>
#include "process.hpp"
#include "scheduling.hpp"
#include "test.hpp"

std::vector<process> start_input();
std::vector<process> test_data();
void show_help();

int main(int argc, char **argv)
{
  std::vector<process> process_list;
  
   if (argc == 2)
    {
      if (strcmp(argv[1], "-t") == 0)
	{
	  run_tests(false);
	  return 0;
	}

      if (strcmp(argv[1], "-to") == 0)
	{
	  run_tests(true);
	  return 0;
	}
      
      if (strcmp(argv[1], "-h") == 0)
	{	 
	  show_help();
	  return 0;
	}

      bool valid_parameter = false;
      if (strcmp(argv[1], "-a") == 0)
	{
	  process_list = test_data();
	  valid_parameter = true;
	}      

      if (!valid_parameter)
	{
	  std::cout<<"Parameter not defined!"<<std::endl;
	  show_help();
	  return 0;
	}
    }

   else if (argc == 1)
     {
       process_list = start_input();
     }

   else
     {
       std::cout<<"Invalid starting parameters!"<<std::endl;
       show_help();
       return 0;
     }
  
  fcfs(&process_list);
  sjf(&process_list);
  srtf(&process_list);
  rr(&process_list, 2);
  ps_non(&process_list);
  ps_pre(&process_list);
  ls(&process_list);
  
  return 0;
}

std::vector<process> start_input()
{   
  std::vector<process> processes;
  std::cout<<"\n**************************************************"<<std::endl;
  std::cout<<"  Scheduling"<<std::endl;
  std::cout<<"**************************************************"<<std::endl;
  std::cout<<"After every entry press enter and to finish press: \nEsc + Enter\n"<<std::endl;
  std::cout<<"----------------------------"<<std::endl;
  bool creating_processes = true, successful_input = false;
  int id=0, attribute_value[3];
  std::string attribute_name[] = {"Insertion point", "       Duration", "       Priority"};

  
  while(creating_processes)
    {
      std::cout<<"Values for Process "<<id<<std::endl;
					    
      for(int i=0; i<3; i++)
	{
	  std::cout<<attribute_name[i]<<": ";	  

	  while(std::cin>>attribute_value[i])
	    {
	      successful_input = true;
	      break;
	    }
	  if(!successful_input)
	    {
	    std::cout<<"\n\n\nExited because esc!"<<std::endl;
	    creating_processes = false;	  
	    std::cout<<"Finished input with "<<id<<" processes!"<<std::endl;
	    std::cout<<"^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n"<<std::endl;
	    break;
	  }
	  successful_input = false;
	}
      if(creating_processes)
	{
	  process new_process(id, attribute_value[0], attribute_value[1], attribute_value[2]);
	  processes.push_back(new_process);
	  id++;
	     
	  std::cout<<"Process successfully created"<<std::endl;
	  std::cout<<"----------------------------\n"<<std::endl;
	}
    }
  std::cout<<"Entered: "<<std::endl;
  for(process one : processes)
    {
      std::cout<<"Id:"<<one.getId()<<" duration: "<<one.getDuration()<<" time: "<<one.getRemainingTime()<<std::endl;
    }

  return processes;
}


std::vector<process> test_data()
{
  std::vector<process> process_list;
  process p1(1,1,4,4);
  process p2(2,2,2,5);
  process p3(3,3,2,3);
  process p4(4,4,2,5);
  process_list.push_back(p1);
  process_list.push_back(p2);
  process_list.push_back(p3);
  process_list.push_back(p4);

  std::cout<<"\nInput data: \n";
  for(process p : process_list)
    std::cout<<"id: "<<p<<", insert: "<<p.getInsertionTime()<<", remaining: "<<p.getRemainingTime()<<std::endl;
  
  return process_list;
}

void show_help()
{
  std::cout<<"\nSimple scheduling program\n*************************\n"<<std::endl;
  std::cout<<"Parameters: \n^^^^^^^^^^ \n -a \t use generated data \n -h \t show help\n"<<
    " -t \t run predefined tests\n -to \t run tests an print calculated results\n"<<std::endl;
}

