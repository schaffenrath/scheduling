#ifndef PROCESS_H
#define PROCESS_H

#include <iostream>

class process
{
private:
  int id;
  int duration;
  int remaining_time;
  int insertion_time;
  int priority;
  int tickets;
  
public:
  process();
  process(int id_);
  process(int id_, int insertion_time_, int duration_, int priority_);
  friend std::ostream &operator<<(std::ostream &str, const process &p);
  void setRemainingTime(int remaining_time_);
  void setDuration(int duartion_);
  void setInsertionTime(int insertion_time_);
  void setTickets(int tickets_);
  int getId();
  int getDuration();
  int getRemainingTime();
  int getInsertionTime();
  int getPriority();
  int getTickets();
};

#endif 
