#ifndef SCHEDULE_FUNCTIONS_H
#define SCHEDULE_FUNCTIONS_H

#include "process.h"
#include "queue.h"
#include "scheduling.h"

queue_pos_t *add_equal_insertion_proc (process_list_t *proc_list, queue_pos_t *queue,
				       short *copied_list);
queue_pos_t *add_queue_node (process_list_t *proc_list, queue_pos_t *queue,
			     unsigned short new_position);
void split_running_process (queue_pos_t **queue);
process_list_t *find_shortest (process_list_t *process_list);
process_list_t *find_highest_priority (process_list_t *process_list);
process_list_t *find_lottery_winner(process_list_t *process_list, unsigned short total_tickets);
void update_position (queue_pos_t **queue);
void print_queue(queue_t *head);
short check_tickets(process_list_t *proc_list_head);
#endif
