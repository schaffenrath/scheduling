#include <ctime>
#include <cmath>
#include <algorithm>
#include <climits>
#include <cstdio>
#include <iostream>
#include <vector>
#include <random>
//#include "generate_processes.h"
#include "generate_scheduling2.h"

using namespace std;


/*
 *	This file contains the functions to create random processes with a specific amount of conflicts. The attributes for the processes are 
 *  generated here and the verify-functions from generate_scheduling.cpp are called. 
 */

random_device rd;
mt19937 gen(rd());


bool is_smaller(unsigned short x, unsigned short y)
{
  return x < y;
}

unsigned short generate_unique_id(std::vector<process_t> *proc_list, unsigned short max)
{
  uniform_real_distribution<double> randoo(0.0,(double) max);
  if (proc_list->empty())
    return (unsigned short) randoo(gen);

  bool unique_id_found = false;
  bool exists_already = false;
  std::vector<unsigned short> possible_ids;
  for (size_t i=0; i < max; i++)
    {
      for (process_t proc : *proc_list)
	{
	  if (proc.id == i)
	    exists_already = true;
	}
      if (!exists_already)
	possible_ids.push_back(i);
      exists_already = false;
    }

  uniform_real_distribution<double> rando(0.0,(double) possible_ids.size());

  return possible_ids[(size_t) rando(rd)];
}

unsigned short generate_insertion_time(std::vector<process_t> *proc_list, bool should_conflict,
				       unsigned short amount, unsigned short max,
				       unsigned short quantum, unsigned short *arrival_helper,
				       unsigned short *tolerance)
{ 
  bool found_valid_values = false;
  std::vector<unsigned short> possible_arrivals;
  std::vector<unsigned short> test_values;
  unsigned short selected = 0, apply_random_tolerance = 0;

  check_rr(*proc_list, quantum, &test_values); 
  std::sort(test_values.begin(), test_values.end(), is_smaller);
  test_values.erase(unique(test_values.begin(), test_values.end()), test_values.end());  
  
  if (proc_list->empty())
    {
      found_valid_values = true;
      *tolerance = max - amount;
      if (*tolerance > 0)
	{
	  uniform_real_distribution<double> rando(1.0, (double) 100.0);
	  if ((short)rando(gen) <= (short)(*tolerance/max)*200)
	    apply_random_tolerance = *tolerance/amount + (*tolerance/amount)/(*tolerance/max);
	}

      for (int i=0; i <= apply_random_tolerance; i++)
	possible_arrivals.push_back(i);
    }

  else
    {
      if (*tolerance > 0)
	{
	  uniform_real_distribution<double> rando(1.0,(double) 100);
	  unsigned short tolerance_prop = (((double)*tolerance/max)*200.0);
	  if ((unsigned short)rando(gen) <= tolerance_prop){
	    apply_random_tolerance = *tolerance/amount + (*tolerance/amount)/(tolerance_prop/100);
	  }
	}

      bool found_conflict = false;
      for (int i = *arrival_helper; i <= *arrival_helper + apply_random_tolerance; i++)
	{
	  for (size_t j = 0; j < test_values.size() && test_values[j] < i; j++)
	    {
	      if (i ==  test_values[j])
		found_conflict = true;
	    }
	  
	  if (!found_conflict)
	    {
	      possible_arrivals.push_back(i);
	      found_valid_values = true;
	    }
	  found_conflict = false;
	}	
    }

  if (!found_valid_values)
    {
      printf("no valid values for rr\n");
      exit(0);
    }

  uniform_real_distribution<double> rando(0.0,(double) possible_arrivals.size());
  selected = possible_arrivals[(size_t) rando(gen)];
  *arrival_helper = selected+1;

  return selected;
}

unsigned short generate_remaining_time(std::vector<process_t> *proc_list,
				       process_t *new_proc,
				       bool should_conflict,
				       unsigned short amount,
				       unsigned short max,
				       unsigned short conflict_counter,
				       unsigned short quantum)
{

  std::vector<unsigned short> possible_remaining;
  std::vector<unsigned short> conflict_remaining;

  check_sjf(*proc_list, new_proc, &conflict_remaining);
  check_srtf(*proc_list, new_proc, &conflict_remaining);
  
  //remove duplicates from list of conflict values
  std::sort(conflict_remaining.begin(), conflict_remaining.end(), is_smaller);
  conflict_remaining.erase(unique(conflict_remaining.begin(), conflict_remaining.end()), conflict_remaining.end());
  
  bool should_add = true;
  size_t upper_bound = (amount/max) <= 0.6 ? max/2 : max;
  for (size_t i=1; i < upper_bound; i++)
    {
      for (size_t j=0; j < conflict_remaining.size(); j++)
	{
	  if (i == conflict_remaining[j])
	    should_add = false; 
	}
      if (should_add)
	possible_remaining.push_back(i);
      should_add = true;
    }
  
  if (!should_conflict)
    {
      if (possible_remaining.empty()) {
	printf("no valid values for duration!\n"); exit(0);}
      
      uniform_real_distribution<double> rando(0.0,(double) possible_remaining.size());
      return possible_remaining[(size_t) rando(gen)];
    }
  
  uniform_real_distribution<double> rando(0.0,(double) conflict_remaining.size());
  return conflict_remaining[(size_t) rando(gen)];  
}


unsigned short generate_priority(std::vector<process_t> *proc_list, process_t *new_proc,
				 bool should_conflict)
{
  std::vector<unsigned short> possible_priority;
  std::vector<unsigned short> conflict_priority;

  check_ps_non(*proc_list, new_proc, &conflict_priority);
  check_ps_pre(*proc_list, new_proc, &conflict_priority);
  
  //remove duplicates from list of conflict values
  std::sort(conflict_priority.begin(), conflict_priority.end(), is_smaller);
  conflict_priority.erase(unique(conflict_priority.begin(), conflict_priority.end()), conflict_priority.end());
  
  bool should_add = true;
  for (size_t i=1; i < 10; i++)
    {
      for (size_t j=0; j < conflict_priority.size(); j++)
	{
	  if (i == conflict_priority[j])
	    should_add = false; 
	}
      if (should_add)
	possible_priority.push_back(i);
      should_add = true;
    }
  
  if (!should_conflict)
    {
      if (possible_priority.empty()){
	printf("no valid values for priority!\n"); exit(0);}
      
      uniform_real_distribution<double> rando(0.0,(double) possible_priority.size());
      return possible_priority[(size_t) rando(gen)];
    }
  
  uniform_real_distribution<double> rando(0.0,(double) conflict_priority.size());
  return conflict_priority[(size_t) rando(gen)];  


}


bool is_earlier_and_shorter2(process_t x, process_t y)
{
  //if insertion and duration are equal, order via id
  if (x.insertion_time == y.insertion_time)
    {
      if (x.remaining_time == y.remaining_time)
	{
	  return false;
	}
      else
	{
	  return x.remaining_time < y.remaining_time;
	}
    }
  
  return x.insertion_time < y.insertion_time;
}

void generate_procs(process_list_t *head, unsigned short amount, unsigned short max, unsigned short quantum, unsigned short alt_solutions)
{
  bool done = false, should_conflict = false;
  unsigned short created = 0, conflict_counter = 0, tolerance = 0, arrival_helper = 0;
  std::vector<process_t> new_proc_list;
  
  while (!done)
    {
      should_conflict = false;
      if (alt_solutions > 0 && created >= amount - alt_solutions)
	should_conflict = true;
      
      process_t new_proc;
      new_proc.id = generate_unique_id(&new_proc_list, max);
      new_proc.insertion_time = generate_insertion_time(&new_proc_list, should_conflict, amount,
							max, quantum, &arrival_helper, &tolerance);
      new_proc.duration = generate_remaining_time(&new_proc_list, &new_proc, should_conflict,
						  amount, max, conflict_counter, quantum);
      //new_proc.priority = generate_priority(&new_proc_list, &new_proc, should_conflict);
      new_proc.tickets = 0;
      new_proc.quantum_usage = 0;

      new_proc_list.push_back(new_proc);
      created++;
            
      if (should_conflict)
	conflict_counter++;
      
      if (created >= amount)
	done = true;

    }

  std::sort(new_proc_list.begin(), new_proc_list.end(), is_earlier_and_shorter2);
  
  for (process_t proc : new_proc_list)
    {
      std::cout<<"\nid: "<<proc.id<<"\n";
      std::cout<<"insertion: "<<proc.insertion_time<<"\n";
      std::cout<<"duration: "<<proc.duration<<"\n";
      std::cout<<"priority: "<<proc.priority<<"\n";
    }
}


int main()
{
  process_list_t a;
  generate_procs(&a, 10, 20, 2, 0);
  return 0;
}
