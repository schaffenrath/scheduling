#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <cmath>
#include "schedule_functions.h"

using namespace std;


/*
 *  This file contains the helper functions for the scheduling in scheduling.cpp. These functions handle the insertion of a new process, 
 *  split a process if a preemtive algorithm was chosen, find specific processes... 
 */ 


queue_pos_t *add_equal_insertion_proc(process_list_t *proc_list, queue_pos_t *queue)
{
  unsigned short new_tickets = 0;
  if (proc_list == NULL && PTR == NULL)
    return NULL;
  
  process_list_t *processes_in_queue = NULL;
  process_list_t *node_iterator = NULL;
  unsigned short current_position = 0;

  if (queue->tail == NULL)
    {
      current_position = proc_list->process.insertion_time;
      node_iterator = proc_list;
    }
  
  // if only 1 process is in queue
  else if (queue->tail->processes == NULL)
    {
      if (PTR == NULL)
	return NULL;

      processes_in_queue = proc_copy(queue->tail->processes, &queue->tail->processes);
      
      current_position = PTR->process.insertion_time;
      node_iterator = PTR;
      new_tickets = queue->tail->total_tickets;
    }

  else
    {
      process_list_t *new_running_pointer = RUNNING;
      current_position = PTR->process.insertion_time;
      new_tickets = queue->tail->total_tickets;

      //copy list of processes previously in the queue 
      processes_in_queue = proc_copy(queue->tail->processes, &new_running_pointer);
      
      if (PTR != NULL && 
	  PTR->process.insertion_time <= current_position)
	{
	  node_iterator = PTR;
	  while (node_iterator != NULL && node_iterator->process.insertion_time <= current_position)
	    {
	      proc_append(&processes_in_queue, &(node_iterator->process));
	      new_tickets += node_iterator->process.tickets;
	      node_iterator = node_iterator->next;
	    }
	}

      queue_append(&queue, processes_in_queue);      
      
      if (node_iterator != NULL)
	PTR = node_iterator;

      else
	PTR = NULL;     

      queue->tail->total_tickets = new_tickets;
      RUNNING = new_running_pointer;
      
      return queue;
    }

  //add all processes earliest insertion time
  while (node_iterator != NULL && node_iterator->process.insertion_time == current_position)
    {
      proc_append(&processes_in_queue, &(node_iterator->process));
      new_tickets += node_iterator->process.tickets;
      node_iterator = node_iterator->next;
    }

  queue_append(&queue, processes_in_queue);
  queue->tail->total_tickets = new_tickets;
  PTR = node_iterator;

  return queue;
}


queue_pos_t *add_queue_node(process_list_t *proc_list, queue_pos_t *queue,
			    unsigned short new_position)
{
  /* add elements from process list if queue empty or insertion of process
   * was before or right at current position */
  if (queue->tail == NULL || queue->tail->processes == NULL ||
      (PTR != NULL &&
       new_position >= PTR->process.insertion_time))
    
    {
      queue = add_equal_insertion_proc(proc_list, queue);
      
      if (queue != NULL)
	POSITION = new_position;
    }

  else
    {
      process_list_t *processes_in_queue = NULL;
      process_list_t *running_ptr = RUNNING;
      processes_in_queue = proc_copy(queue->tail->processes, &running_ptr);
      queue_append(&queue, processes_in_queue);
      PTR = PREV_PTR;
      RUNNING = running_ptr;
      queue->tail->total_tickets = queue->tail->prev->total_tickets;
      POSITION = new_position;
    }
  
  return queue;
}


void split_running_process(queue_pos_t **queue)
{
  queue_pos_t *queue_pointer = *queue;
  process_t split_process = queue_pointer->tail->running_process->process;
  split_process.insertion_time = queue_pointer->tail->position;
   
  proc_remove_process(&queue_pointer->tail->processes,
		      queue_pointer->tail->running_process->process.id);

  if (split_process.remaining_time <= 0)
    {
      split_process.remaining_time = 0;
      queue_pointer->tail->total_tickets -= split_process.tickets;
    }
  
  else
    proc_append(&queue_pointer->tail->processes, &split_process); 
  
  *queue = queue_pointer;
}


process_list_t *find_shortest(process_list_t *process_list)
{
  if (process_list == NULL)
    return NULL;
  
  process_list_t *proc_iterator = process_list;
  process_list_t *shortest_process = process_list;

  while (proc_iterator != NULL)
    {
      if (proc_iterator->process.remaining_time == shortest_process->process.remaining_time)	
	if(proc_iterator->process.id < shortest_process->process.id)
	  shortest_process = proc_iterator;	
	  
      if (proc_iterator->process.remaining_time < shortest_process->process.remaining_time)
	shortest_process = proc_iterator;

      proc_iterator = proc_iterator->next;
    }
  
  return shortest_process;
}


process_list_t *find_highest_priority(process_list_t *process_list)
{
  if (process_list == NULL)
    return NULL;

  process_list_t *proc_iterator = process_list;
  process_list_t *highest_priority = process_list;

  while (proc_iterator != NULL)
    {
      if (proc_iterator->process.priority == highest_priority->process.priority)
	if (proc_iterator->process.id < highest_priority->process.id)
	  highest_priority = proc_iterator;
      
      if (proc_iterator->process.priority < highest_priority->process.priority)
	highest_priority = proc_iterator;	

      proc_iterator = proc_iterator->next;
    }
  return highest_priority;
}


process_list_t *find_lottery_winner(process_list_t *process_list, unsigned short total_tickets)
{
  if (process_list == NULL || total_tickets == 0)
    return NULL;


  //Carta's implementation Park-Miller pseudo-random number generator (Minimal standard)
  unsigned int seed = (unsigned int) clock();
  unsigned int lo, hi;
  unsigned short rand;

  lo = 16807 * (seed & 0xFFFF);
  hi = 16807 * (seed >> 16);

  lo += (hi & 0x7FFF) << 16;
  lo += hi >> 15;

  if (lo > 0x7FFFFFFF) lo -= 0x7FFFFFFF;
  rand = (unsigned short)lo % total_tickets;
  
  process_list_t *ticket_iterator = process_list;
  unsigned short ticket_sum = 0;
  while (ticket_iterator->next != NULL && ticket_sum < rand)
    {
      ticket_sum += ticket_iterator->process.tickets;
      if (ticket_sum < rand)
	ticket_iterator = ticket_iterator->next;
    }
  
  return ticket_iterator;
}


void update_position(queue_pos_t **queue)
{
  queue_pos_t *queue_handler = *queue;
  if (queue_handler->tail->prev != NULL)    
    queue_handler->tail->position = (queue_handler->tail->prev->position +
				     queue_handler->tail->prev->running_process->process.remaining_time);    
  else
    queue_handler->tail->position = queue_handler->tail->running_process->process.insertion_time;
}

void print_queue(queue_t *head)
{
  printf("\nqueue list:\n");
  queue_t *it = head;
  while (it != NULL)
    {
      printf("position: %hu\n",it->position);
      printf("proc: ");
      process_list_t *ai = it->processes;
      while (ai != NULL)
	{
	  printf("%hu ",ai->process.id);
	  ai = ai->next;
	}
      it = it->next;
      printf("\n");
    } printf("\n");

}

short check_tickets(process_list_t *proc_list_head)
{
  process_list_t *it = proc_list_head;
  while (it != NULL)
    {
      if (it->process.tickets != 0)	
	  it = it->next;
      else
	return 0;
    }
  return 1;
}
