#include <cstdlib>
#include "queue.h"


/*
 *  This file contains the functions to handle the queue. The queue should represent a queue from a real process schedular, where 
 *  one node represents the state of the queue for a given time. All nodes are part of a double linked list. 
 */


void queue_append(queue_pos_t **queue, process_list_t *process_list)
{
  queue_pos_t *new_queue = *queue;
  queue_t *new_node = (queue_t*) malloc(sizeof(queue_t));
  new_node->processes = process_list;
  new_node->position = 0;
  new_node->running_process = NULL;
  new_node->proc_list_pointer = NULL;
  new_node->next = NULL;
  new_node->prev = NULL;
  
  if (new_queue->tail == NULL)
    { 
      new_node->next = NULL;
      new_node->prev = NULL;
      new_queue->head = new_node;
      new_queue->tail = new_node;      
      *queue = new_queue;
    }
  else
    {
      new_queue->tail->next = new_node;
      new_node->prev = new_queue->tail;
      new_queue->tail = new_node;
      *queue = new_queue;
    }
}


queue_t *queue_copy_last_node(queue_t *queue)
{
  if (queue == NULL)
    return NULL;

  queue_t *new_node = (queue_t*) malloc(sizeof(queue_t));
  new_node->position = queue->position;
  new_node->total_tickets = queue->total_tickets;
  new_node->running_process = queue->running_process;
  new_node->proc_list_pointer = queue->proc_list_pointer;
  new_node->processes = proc_copy(queue->processes, &new_node->running_process);
  new_node->prev = queue;
  new_node->next = NULL;
  queue->next = new_node;
  
  return new_node;
}


void queue_clear(queue_pos_t *queue)
{
  if (queue == NULL)
    return;

  if (queue->head->next == NULL)
    {
      proc_clear(queue->head->processes);
      free(queue->head);
      return;
    }
  
  queue->head = queue->head->next; 
  
  while (queue->head->next != NULL)
    {    
      proc_clear(queue->head->prev->processes);
      free(queue->head->prev);
      queue->head = queue->head->next;     
    }

  proc_clear(queue->head->prev->processes);
  free(queue->head->prev);  

  proc_clear(queue->head->processes);
  free(queue->head);
}
