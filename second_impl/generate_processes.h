#ifndef GENERATE_PROCESSES_H
#define GENERATE_PROCESSES_H

#include "process.h"

void generate_procs(process_list_t *head, unsigned short amount, unsigned short max, unsigned short alt_solutions);

#endif
