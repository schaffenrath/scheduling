#include <cstdio>
#include <cstdlib>
#include <climits>
#include <iostream>
#include "scheduling.h"
#include "metric.h"

using namespace std;


/*
 *  This file contains all scheduling algorithms.  
 * 
 */

queue_pos_t *fcfs_next (process_list_t *proc_list, queue_pos_t *queue)
{
  unsigned short new_position = proc_list->process.insertion_time;
  int should_delete = -1;

  if (queue->tail != NULL)
    {
      if (RUNNING == NULL && PTR != NULL)
        new_position = PTR->process.insertion_time;

      else if (PTR != NULL && PTR->process.insertion_time < POSITION + REMAINING)
        {
          new_position = PTR->process.insertion_time;
          REMAINING -= new_position - POSITION;
        }
      else
        {
          if (PTR != NULL)
            {
              new_position = PTR->process.insertion_time;
              should_delete = (int) REMAINING - ((int) new_position - (int) POSITION);


              if (should_delete <= 0)
                {
                  new_position = POSITION + REMAINING;
                  REMAINING = 0;
                }
              else
                REMAINING = should_delete;
            }
          else
            {
              new_position = POSITION + REMAINING;
              REMAINING = 0;
            }

          if (REMAINING == 0)
            should_delete = (int) ID;
        }
    }

  queue = add_queue_node (proc_list, queue, new_position);

  if (queue != NULL)
    {
      if (should_delete >= 0)
        {
          proc_remove_process (&queue->tail->processes, (unsigned short) should_delete);

          if (queue->tail->processes == NULL && PTR == NULL)
            return NULL;
        }

      RUNNING = queue->tail->processes;
    }

  return queue;
}

queue_pos_t *sjf_next (process_list_t *proc_list, queue_pos_t *queue)
{
  unsigned short new_position = proc_list->process.insertion_time;
  int should_delete = -1;

  if (queue->tail != NULL)
    {
      if (RUNNING == NULL && PTR != NULL)
        new_position = PTR->process.insertion_time;

      else if (PTR != NULL && PTR->process.insertion_time < POSITION + REMAINING)
        {
          new_position = PTR->process.insertion_time;
          REMAINING -= new_position - POSITION;
        }
      else
        {
          if (PTR != NULL)
            {
              new_position = PTR->process.insertion_time;
              should_delete = (int) REMAINING - ((int) new_position - (int) POSITION);


              if (should_delete <= 0)
                {
                  new_position = POSITION + REMAINING;
                  REMAINING = 0;
                }
              else
                REMAINING = should_delete;
            }
          else
            {
              new_position = POSITION + REMAINING;
              REMAINING = 0;
            }

          if (REMAINING == 0)
            should_delete = (int) ID;
        }
    }

  queue = add_queue_node (proc_list, queue, new_position);

  if (queue != NULL)
    {
      if (should_delete >= 0)
        {
          proc_remove_process (&queue->tail->processes, (unsigned short) should_delete);

          if (queue->tail->processes == NULL && PTR == NULL)
            return NULL;
        }

      if (RUNNING == NULL || should_delete >= 0)
        RUNNING = find_shortest (queue->tail->processes);

      else
        RUNNING = PREV_RUNNING;
    }

  return queue;
}

queue_pos_t *srtf_next (process_list_t *proc_list, queue_pos_t *queue)
{
  int should_delete = -1;
  unsigned short new_position = proc_list->process.insertion_time;

  if (queue->tail != NULL)
    {
      if (RUNNING == NULL && PTR != NULL)
        new_position = PTR->process.insertion_time;

      else if (PTR != NULL && PTR->process.insertion_time < POSITION + REMAINING)
        {
          new_position = PTR->process.insertion_time;
          REMAINING -= new_position - POSITION;
        }
      
      else
        {
          if (PTR != NULL)
            {
              new_position = PTR->process.insertion_time;
              should_delete = (int) REMAINING -
                      ((int) new_position - (int) POSITION);

              if (should_delete <= 0)
                {
                  new_position = POSITION + REMAINING;
                  REMAINING = 0;
                }
              else
                REMAINING = should_delete;
            }
          else
            {
              new_position = POSITION + REMAINING;
              REMAINING = 0;
            }

          if (REMAINING == 0)
            should_delete = (int) ID;
        }
    }

  queue = add_queue_node (proc_list, queue, new_position);

  if (queue == NULL)
    return NULL;

  if (should_delete >= 0)
    {
      proc_remove_process (&queue->tail->processes, (unsigned short) should_delete);
      
      RUNNING = PREV_RUNNING;
      
      if (queue->tail->processes == NULL && PTR == NULL)
        return NULL;
    }

  // for first execution and if no process is running
  if (queue->tail->prev == NULL || RUNNING == NULL)
    {
      RUNNING = find_shortest (queue->tail->processes);
      POSITION = new_position;
      return queue;
    }

  // if all initial processes were added to queue
  if (PTR == NULL && PREV_PTR == NULL)
    {
      int time_remaining = (int) PREV_REMAINING - ((int) new_position - (int) POSITION);

      if (time_remaining >= 0)
        PREV_REMAINING = (unsigned short) time_remaining;

      else
        PREV_REMAINING = 0;

      RUNNING = find_shortest (queue->tail->processes);
      POSITION = new_position;

      return queue;
    }

  POSITION = new_position;

  unsigned short prev_running_remaining_time =
          PREV_REMAINING - (new_position - queue->tail->prev->position);

  process_list_t *shortest_process = find_shortest (queue->tail->processes);

  // if running process is done before next one starts
  if ((POSITION + REMAINING) <= PREV_PTR->process.insertion_time)
    {
      proc_remove_process (&queue->tail->processes, PREV_RUNNING->process.id);

      shortest_process = find_shortest (queue->tail->processes);
      POSITION = new_position;

    } // if shorter process is in queue
  else if (prev_running_remaining_time > shortest_process->process.remaining_time)
    {
      POSITION = new_position;
      split_running_process (&queue);

      shortest_process = find_shortest (queue->tail->processes);
    }
  else
    {
      POSITION = new_position;
      int time_remaining = (int) REMAINING -
              ((int) new_position - (int) queue->tail->prev->position);


      if (time_remaining < 0)
        {
          REMAINING = 0;
          proc_remove_process (&queue->tail->processes, PREV_RUNNING->process.id);

          shortest_process = find_shortest (queue->tail->processes);
        }
    }

  RUNNING = shortest_process;
  return queue;
}



queue_pos_t *rr_next(process_list_t *proc_list, queue_pos_t *queue, unsigned short quantum) {
    short new_proc_inserted = 0;
    int new_position = 0;
    static unsigned short quantum_time = 0;
    
    if (queue->tail == NULL)
      {
	new_position = proc_list->process.insertion_time;
	new_proc_inserted = 1;
      }   

    else if (RUNNING == NULL && PTR != NULL)
      {
	new_position = PTR_INSERTION;
	quantum_time = new_position + quantum;
      }
    
    else if (POSITION + REMAINING < quantum_time)      
      {
	new_position = POSITION + REMAINING;
      }
      
    else
      {
	if (POSITION == quantum_time)
	  new_position = quantum_time + quantum;

	else
	  new_position = quantum_time;
      }

    quantum_time = new_position;
    
    if (queue->tail != NULL && PTR != NULL && (int)PTR->process.insertion_time <= new_position)
      {
	new_position = PTR_INSERTION;
	new_proc_inserted = 1;
      }  
    
    if (new_proc_inserted)      
      queue = add_queue_node(proc_list, queue, new_position);     

    else
      queue->tail = queue_copy_last_node(queue->tail);  
    
    if (queue->tail->prev == NULL || RUNNING == NULL)
      {
	RUNNING = queue->tail->processes;
	POSITION = new_position;
	quantum_time = new_position + quantum;
	return queue;
      }

    if (new_proc_inserted && new_position != quantum_time)
      {
	RUNNING = PREV_RUNNING;       	
	REMAINING -= (new_position - PREV_POSITION);
	POSITION = new_position;
	
	return queue;
      }

    if (POSITION + REMAINING == new_position)
      {
	proc_remove_process(&queue->tail->processes, PREV_RUNNING->process.id);
	RUNNING = queue->tail->processes;
	POSITION = new_position;

	quantum_time = new_position + quantum;
	
	if (RUNNING == NULL && PTR == NULL)
	  return NULL;
	
	return queue;
      }
    
    else
      {
	POSITION = new_position;
	
	if ((int)REMAINING - (new_position - (int)PREV_POSITION) <= 0)
	  REMAINING = 0;
	else
	  REMAINING -= (new_position - (int)PREV_POSITION);

	split_running_process(&queue);
	RUNNING = queue->tail->processes;       

	if (RUNNING == NULL && PTR == NULL)
	  return NULL;

	quantum_time = new_position + quantum;
      }    

    return queue;
}




queue_pos_t *ps_next (process_list_t *proc_list, queue_pos_t *queue, unsigned short preemtive_type)
{
  int should_delete = -1;
  unsigned short new_position = 0;

  printf("\nin ps_next\n");
  
  if (queue->tail != NULL)
    {      
      if (RUNNING == NULL && PTR != NULL)
	new_position = PTR->process.insertion_time;	  
      
      else if (PTR != NULL && PTR->process.insertion_time < POSITION + REMAINING)
        {
	  new_position = PTR->process.insertion_time;
	  REMAINING -= new_position - POSITION;
        }

      else
        {
          if (PTR != NULL)
            {
	      new_position = PTR->process.insertion_time;
	      should_delete = (int) REMAINING -
		((int) new_position - (int) POSITION);

	      if (should_delete <= 0)
		{
		  new_position = POSITION + REMAINING;
		  REMAINING = 0;
		}
	      
	      else
		REMAINING = should_delete;
	    }
          else
            {
              new_position = POSITION + REMAINING;
              REMAINING = 0;
            }

          if (REMAINING == 0)
            should_delete = (int) ID;
        }
    }

  printf("before add queue with pos: %d\n", new_position);
  queue = add_queue_node (proc_list, queue, new_position);
  printf("after add queue\n");

  if (queue == NULL)
    return NULL;
  printf("queue not null\n");

  if (queue->tail->processes == NULL && PTR == NULL){
    printf("running and ptr null!\n");
    return NULL;}
  printf("running and ptr not null\n");
  
  if (should_delete >= 0)
    {
      printf("in delete!\n");
      proc_remove_process (&queue->tail->processes, (unsigned short) should_delete);

      RUNNING = PREV_RUNNING;

      if (queue->tail->processes == NULL && PTR == NULL)
        return NULL;
      printf("after delete\n");
    }

  // for first execution
  if (queue->tail->prev == NULL || RUNNING == NULL)
    {
      printf("in prev or run null\n");
      RUNNING = find_highest_priority (queue->tail->processes);
      POSITION = INSERTION;
      printf("after prev or run null\n");
      return queue;
    }

  // if all initial processes were added to queue
  if (PTR == NULL && PREV_PTR == NULL)
    {
      printf("in ptr and prev null\n");
      int time_remaining = (int) PREV_REMAINING - ((int) new_position - (int) POSITION);
      if (time_remaining >= 0)
	PREV_REMAINING = (unsigned short) time_remaining;
      else
	PREV_REMAINING = 0;
      
      RUNNING = find_highest_priority (queue->tail->processes);
      POSITION = new_position;
      printf("after ptr and prev null\n");
      return queue;
    }

  printf("in later section\n");
  //new_position =  PREV_PTR->process.insertion_time;    
  POSITION = new_position;

  process_list_t *priority_process = NULL;

  if (preemtive_type == PREEMTIVE)
    priority_process = find_highest_priority (queue->tail->processes);

  else
    priority_process = RUNNING;

  // if running process is done before next one starts
  if ((POSITION + REMAINING) <= PREV_PTR->process.insertion_time)
    {
      printf("in done before start!\n");
      if (queue->tail->processes == NULL)
	printf("no procs in queue!\n");
      else
	printf("procs are in queue\n");
      POSITION = new_position;
      proc_remove_process (&queue->tail->processes, PREV_RUNNING->process.id);

      priority_process = find_highest_priority (queue->tail->processes);
    }
  // if process with higher priority was found
  else if (preemtive_type == PREEMTIVE &&
           PREV_RUNNING->process.priority > priority_process->process.priority)
    {
      printf("in else if\n");
      REMAINING -= (new_position - POSITION);
      POSITION = new_position;
      split_running_process (&queue);

      priority_process = find_highest_priority (queue->tail->processes);
    }
  
  else
    {
      printf("in else\n");
      POSITION = new_position;
      REMAINING = 0;
      proc_remove_process (&queue->tail->processes, PREV_RUNNING->process.id);

      priority_process = find_highest_priority (queue->tail->processes);      
    }

  RUNNING = priority_process;
  if (RUNNING == NULL)
    printf("before return running == null!\n");
  else
  printf("before return with proc: %d\n", RUNNING->process.id);
  return queue;
}

queue_pos_t *ls_next (process_list_t *proc_list, queue_pos_t *queue, unsigned short quantum)
{
  short new_proc_inserted = 0;
  int new_position = 0;
  static unsigned short quantum_time = 0;

  if (queue->tail == NULL)
      {
	new_position = proc_list->process.insertion_time;
	new_proc_inserted = 1;
      }   

    else if (RUNNING == NULL && PTR != NULL)
      {
	new_position = PTR_INSERTION;
	quantum_time = new_position + quantum;
      }
    
    else if (RUNNING != NULL && POSITION + REMAINING < quantum_time + quantum)      
      {
	new_position = POSITION + REMAINING;
      }
      
    else
      {
	if (POSITION == quantum_time) {	  
	  new_position = quantum_time + quantum;
	}

	else 
	  new_position = quantum_time;	
      }

    quantum_time = new_position;
    
    if (queue->tail != NULL && PTR != NULL && (int)PTR->process.insertion_time <= new_position)
      {
	new_position = PTR_INSERTION;
	new_proc_inserted = 1;
      }

    if (queue->tail != NULL && RUNNING == NULL && PTR == NULL)
      return NULL;

    if (new_proc_inserted)      
      queue = add_queue_node(proc_list, queue, new_position);     

    else
      queue->tail = queue_copy_last_node(queue->tail);  

  if (queue == NULL)
    return NULL;

  if (queue->tail->prev == NULL || RUNNING == NULL)
    {
      process_list_t *ticket_iterator = proc_list;
      while (ticket_iterator != NULL)
        {
          queue->tail->total_tickets += ticket_iterator->process.tickets;
          ticket_iterator = ticket_iterator->next;
        }

      RUNNING = find_lottery_winner (queue->tail->processes, queue->tail->total_tickets);

      if (RUNNING == NULL)
        return NULL;

      POSITION = RUNNING->process.insertion_time;
      quantum_time = POSITION + quantum;
      return queue;
    }

  if (RUNNING->process.quantum_usage != 0 && POSITION + RUNNING->process.quantum_usage < new_position)
    {
      POSITION += RUNNING->process.quantum_usage;
      REMAINING -= RUNNING->process.quantum_usage; 
      RUNNING = find_lottery_winner (queue->tail->processes, queue->tail->total_tickets);
      return queue;
    }
  
  if (new_proc_inserted && new_position != quantum_time)
    {
      RUNNING = PREV_RUNNING;
      REMAINING -= (new_position - PREV_POSITION);
      return queue;
    }

  if (POSITION + REMAINING == new_position)
    {
      queue->tail->total_tickets -= PREV_RUNNING->process.tickets;
      proc_remove_process (&queue->tail->processes, ID);
      POSITION = new_position;
      quantum_time = new_position + quantum;           

      RUNNING = find_lottery_winner (queue->tail->processes, queue->tail->total_tickets);
      return queue;
    }

  else
    {
      printf("in else\n");
      unsigned short compensation_tickets = 0;
      process_list_t * split_process = NULL;
      
      if (RUNNING->process.quantum_usage != 0)
        POSITION = PREV_POSITION + RUNNING->process.quantum_usage;

      else
	POSITION = new_position;

      if ((int)REMAINING - (int)(POSITION - PREV_POSITION) < 0)
	REMAINING = 0;

      else
	REMAINING -= (POSITION - PREV_POSITION);
      
      split_running_process (&queue);
      RUNNING = PREV_RUNNING;
      
      if (RUNNING->process.quantum_usage != 0 &&
          RUNNING->process.quantum_usage < quantum)
        {
	  printf("in if for quantum usage!\n");
          split_process = proc_find(queue->tail->processes, RUNNING->process.id);

          if (split_process != NULL)
            {
              POSITION = POSITION + RUNNING->process.quantum_usage;
              compensation_tickets = (unsigned short)
                      (1 / (((double) split_process->process.quantum_usage / quantum)));

              queue->tail->total_tickets += compensation_tickets;
              split_process->process.tickets += compensation_tickets;
            }
        }

      RUNNING = find_lottery_winner (queue->tail->processes, queue->tail->total_tickets);

      if (compensation_tickets > 0)
	{
	  split_process->process.tickets -= compensation_tickets;
	  queue->tail->total_tickets -= compensation_tickets;
	}
      
      return queue;
    }
}

queue_t *scheduling (short scheduling_type, process_list_t *proc_list_head)
{
  queue_pos_t *proc_queue = (queue_pos_t*) malloc (sizeof (queue_pos_t));
  proc_queue->head = NULL;
  proc_queue->tail = NULL;
  queue_pos_t *check_result = NULL;
  process_list_t *result_list = NULL;

  queue_pos_t * (*func_ptr)(process_list_t *, queue_pos_t *);

  switch (scheduling_type)
    {
    case FCFS:
      printf ("\nFirst-come First-served:\n");
      func_ptr = &fcfs_next;
      break;

    case SJF:
      printf ("\nShortest job first:\n");
      func_ptr = &sjf_next;
      break;

    case SRTF:
      printf ("\nShortest remaining time first:\n");
      func_ptr = &srtf_next;
      break;

    default:
      printf ("\nWrong scheduling type!\n");
      return 0;
    }

  do
    {
      check_result = (*func_ptr)(proc_list_head, proc_queue);
      if (check_result != NULL)
        {
          proc_queue = check_result;
          if (check_result->tail->running_process != NULL)
            proc_append (&result_list, &(check_result->tail->running_process->process));
        }

    }
  while (check_result != NULL);

  queue_t *it = proc_queue->head;
  while (it != NULL){
      cout << "before pos: "<<it->position<<endl;
      it = it->next;
  }

  print_queue (proc_queue->head);
  metric_t metrics = calc_metrics(proc_queue);
  print_metrics(metrics);
  clear_metrics(metrics.proc_metric_ptr);
  

  queue_clear (proc_queue);
  free (proc_queue);

  printf ("Running: \n");
  unsigned short tmp_save_id = USHRT_MAX;
  process_list_t *ite = result_list;
  while (ite != NULL)
    {
      if (ite->process.id != tmp_save_id)
        printf ("%hu ", ite->process.id);
      tmp_save_id = ite->process.id;
      ite = ite->next;
    }
  printf ("\n");

  proc_clear (result_list);
  return NULL;
}

queue_t *scheduling (short scheduling_type, process_list_t *proc_list_head, unsigned short additional_argument)
{
  queue_pos_t *proc_queue = (queue_pos_t*) malloc (sizeof (queue_pos_t));
  proc_queue->head = NULL;
  proc_queue->tail = NULL;
  queue_pos_t *check_result = NULL;
  process_list_t *result_list = NULL;

  queue_pos_t * (*func_ptr)(process_list_t *, queue_pos_t *, unsigned short);

  switch (scheduling_type)
    {
    case RR:
      printf ("\nRound Robing:\n");
      func_ptr = &rr_next;
      break;

    case PS:
      printf ("\nPriority scheduling:\n");
      func_ptr = &ps_next;
      break;

    case LS:
      printf ("\nLottery scheduling:\n");
      if (!check_tickets(proc_list_head))
      	{
      	  printf("No tickets specified, lottery-scheduling not available!\n");
      	  return 0;
      	}
      func_ptr = &ls_next;
      break;

    default:
      printf ("\nWrong scheduling type!\n");
      return 0;
    }

  do
    {
      check_result = (*func_ptr)(proc_list_head, proc_queue, additional_argument);
      if (check_result != NULL)
        {
          proc_queue = check_result;
          if (check_result->tail->running_process != NULL)
            proc_append (&result_list, &(proc_queue->tail->running_process->process));
        }

    }
  while (check_result != NULL);
  printf("after while\n");

  print_queue(proc_queue->head);
  printf("after print_queue\n");

  metric_t metrics = calc_metrics(proc_queue);
  print_metrics(metrics);
  clear_metrics(metrics.proc_metric_ptr);

  queue_clear (proc_queue);

  printf ("Running: \n");
  unsigned short tmp_save_id = USHRT_MAX;
  process_list_t *ite = result_list;
  while (ite != NULL)
    {
      if (ite->process.id != tmp_save_id)
        printf ("%hu ", ite->process.id);
      tmp_save_id = ite->process.id;
      ite = ite->next;
    }
  printf ("\n");

  proc_clear (result_list);
  free (proc_queue);
  return NULL;
}
