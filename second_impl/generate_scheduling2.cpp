#include <iostream>
#include <vector>
#include <algorithm>
#include <cstdio>
#include "process.h"
#include "generate_scheduling2.h"


/*
 *	This file contains the functions to count the conflicts for FCFS, SJF, SRTF, RR and PS. These functions are called in generate_processes.cpp
 *  to verify the randomly generated attributes of the processes. 
 * 
 *  The scheduling functions in this file differ from the functions in scheduling.cpp, because mainly the process list is here a vector, as where
 *  in scheduling.cpp it is a double linked list. The functions here also don't provide information on the order of execution and no additional data
 *  is stored.
 */


bool is_earlier(process_t x, process_t y)
{
  //if insertionTime is equal, the process with lower id is preferred
  if (x.insertion_time == y.insertion_time)
    return false;
  return x.insertion_time < y.insertion_time;
}

bool is_shorter(process_t x, process_t y)
{
  //if insertion_time and insertion are equal, order via id
  if (x.duration == y.duration)
    {
      if (x.insertion_time == y.insertion_time)
	{
	  return false;
	}
      else
	{
	  return x.insertion_time < y.insertion_time;
	}
    }
  return x.duration < y.duration;
}

bool has_higher_priority(process_t x, process_t y)
{
  //if priority and insertion are equal, order via id
  if (x.priority == y.priority)
    {
      if (x.insertion_time == y.insertion_time)
	{
	  return false;
	}
      else
	{
	  return x.insertion_time < y.insertion_time;
	}
    }
  return x.priority < y.priority;
}

bool is_earlier_and_shorter(process_t x, process_t y)
{
  //if insertion and insertion_time are equal, order via id
  if (x.insertion_time == y.insertion_time)
    {
      if (x.duration == y.duration)
	{
	  return false;
	}
      else
	{
	  return x.duration < y.duration;
	}
    }
  
  return x.insertion_time < y.insertion_time;
}

bool is_earlier_and_higher_priority(process_t x, process_t y)
{
  //if insertion and priority are equal, order via id
  if (x.insertion_time == y.insertion_time)
    {
      if (x.priority == y.priority)
	{
	  return false;
	}
      else
	{
	  return x.priority < y.priority;
	}
    }
  return x.insertion_time < y.insertion_time;
}

void check_sjf(std::vector<process_t> proc_list, process_t *new_process,
	       std::vector<unsigned short> *conflict_remaining)
{
  std::sort(proc_list.begin(), proc_list.end(), is_earlier_and_shorter);
  std::vector<process_t> queue;
  unsigned short position = 0;
  process_t running;
  running.duration = 0;

  while (position < new_process->insertion_time)
    {
      if (!proc_list.empty() && running.duration == 0)
	position = proc_list.front().insertion_time;

      else
	position += running.duration;

      while (!proc_list.empty() && proc_list.front().insertion_time <= position)
      	{
      	  queue.push_back(proc_list.front());
      	  proc_list.erase(proc_list.begin());
      	}
      
      std::sort(queue.begin(), queue.end(), is_shorter);
      
      if (proc_list.empty() && queue.empty())
	{
	  position = new_process->insertion_time;
	  break;
	}

      else if (queue.empty())
	running.duration = 0;

      else if (position <= queue.front().insertion_time)
	{
	  running = queue.front();
	  queue.erase(queue.begin());
	}
    }

  for (process_t proc : queue)
    conflict_remaining->push_back(proc.duration);
    
}


void check_srtf(std::vector<process_t> proc_list, process_t *new_process,
		std::vector<unsigned short> *conflict_remaining)
{
  if (proc_list.empty())
    return;
  
  std::sort(proc_list.begin(), proc_list.end(), is_earlier_and_shorter);
  std::vector<process_t> queue;
  unsigned short position = proc_list.front().insertion_time, prev_position = 0;
  process_t running;
  running.duration = 0;

  while (position <= new_process->insertion_time)
    {
      while (!proc_list.empty() && proc_list.front().insertion_time == position)
	{
	  queue.push_back(proc_list.front());
	  proc_list.erase(proc_list.begin());
	}

      std::sort(queue.begin(), queue.end(), is_shorter);
      
      if (running.duration != 0)
	{
	  if ((int) running.duration - ((int)position - (int)prev_position) < 0)
	    running.duration = 0;
	  else
	    {
	      running.duration -= (position - prev_position);	
	    }
	}

      if (position == new_process->insertion_time)
	{	  
	  if ((int)running.duration - (int)(new_process->insertion_time - position) <= 0)
	    {
	      running.duration = 0;
	      position += 1;
	      break;
	    }
	  
	  else
	    {
	      running.duration -= new_process->insertion_time - position;
	      position += 1;
	      conflict_remaining->push_back(running.duration);
	      break;
	    }
	}

      if (running.duration != 0 && queue.front().duration < running.duration)
	{
	  running.insertion_time = position;
	  queue.push_back(running);
	  running = queue.front();
	  queue.erase(queue.begin());
	}
      
      else if (running.duration == 0)
	{
	  if (queue.empty() && proc_list.empty())	    
	    break;	    
	  
	  else if (!queue.empty())
	    {
	      running = queue.front();
	      queue.erase(queue.begin());
	    }
	}
      
      prev_position = position;

      if (proc_list.empty())
	position += running.duration;
      
      else if (running.duration != 0 && position + running.duration < proc_list.front().insertion_time)
	position += running.duration;

      else
	position = proc_list.front().insertion_time;

      if (position > new_process->insertion_time)
	position = new_process->insertion_time;
    }
  
  if (running.duration != 0)
    conflict_remaining->push_back(running.duration);
  
  for (process_t proc : queue)
    conflict_remaining->push_back(proc.duration); 
}




void check_rr(std::vector<process_t> proc_list, unsigned short quantum, 
	      std::vector<unsigned short> *conflict_arrival)
{
  if (proc_list.empty())
    return;
  
  std::sort(proc_list.begin(), proc_list.end(), is_earlier);
  std::vector<process_t> queue;
  unsigned short position = 0, prev_position = 0, quantum_time = 0, last_arrival = proc_list.back().insertion_time;
  process_t running;
  running.duration = 0;

  while (!proc_list.empty() || !queue.empty())
    {
      if (running.duration != 0)
	{
	  if (position + running.duration >= quantum_time)
	    {
	      position = quantum_time;
	      quantum_time += quantum;
	    }
	  
	  else 
	    {
	      position += running.duration;
	      quantum_time = position + quantum;
	    }
	}
      
      else
	{
	  if (proc_list.empty())	    
	    position = queue.front().insertion_time;	      

	  else	    
	    position = proc_list.front().insertion_time;
	  
	  quantum_time = position + quantum;	  
	}
      
      while (!proc_list.empty() && proc_list.front().insertion_time == position)
	{
	  queue.push_back(proc_list.front());
	  proc_list.erase(proc_list.begin());
	}

      std::sort(queue.begin(), queue.end(), is_earlier);    

      if ((int) running.duration - ((int)position - (int)prev_position) < 0)
	running.duration = 0;
      else
	running.duration -= (position - prev_position);

      if (running.duration != 0 && position == quantum_time - quantum)
	{
	  running.insertion_time = position;	  
	  queue.push_back(running);
	  if (position >= last_arrival)
	    conflict_arrival->push_back(running.insertion_time);
	    
	}

      if (!queue.empty())
	{
	  running = queue.front();
	  queue.erase(queue.begin());
	}

      prev_position = position;
    }
}


void check_ps_non(std::vector<process_t> proc_list, process_t *new_process,
		  std::vector<unsigned short> *conflict_priority)
{
  if (proc_list.empty())
    return;
  
  std::sort(proc_list.begin(), proc_list.end(), is_earlier_and_higher_priority);
  std::vector<process_t> queue;
  unsigned short position = 0;
  process_t running;
  running.duration = 0;

  while (position < new_process->insertion_time)
    {
      if (!proc_list.empty() && running.duration == 0)
	position = proc_list.front().insertion_time;

      else
	position += running.duration;

      while (!proc_list.empty() && proc_list.front().insertion_time <= position)
      	{
      	  queue.push_back(proc_list.front());
      	  proc_list.erase(proc_list.begin());
      	}
      
      std::sort(queue.begin(), queue.end(), has_higher_priority);
      
      if (proc_list.empty() && queue.empty())
	{
	  position = new_process->insertion_time;
	  break;
	}

      else if (queue.empty())
	running.duration = 0;

      else if (position <= queue.front().insertion_time)
	{
	  running = queue.front();
	  queue.erase(queue.begin());
	}
    }
  if (!queue.empty())
    {
      printf("ps non in queue %ld\n", queue.size());
    }

  for (process_t proc : queue)
    conflict_priority->push_back(proc.priority);    
}


void check_ps_pre(std::vector<process_t> proc_list, process_t *new_process,
		  std::vector<unsigned short> *conflict_priority)
{
  if (proc_list.empty())
    return;
  
  std::sort(proc_list.begin(), proc_list.end(), is_earlier_and_higher_priority);
  std::vector<process_t> queue;
  unsigned short position = proc_list.front().insertion_time, prev_position = 0;
  process_t running;
  running.duration = 0;

  while (position <= new_process->insertion_time)
    {
      while (!proc_list.empty() && proc_list.front().insertion_time == position)
	{
	  queue.push_back(proc_list.front());
	  proc_list.erase(proc_list.begin());
	}

      std::sort(queue.begin(), queue.end(), has_higher_priority);
      
      if (running.duration != 0)
	{
	  if ((int) running.duration - ((int)position - (int)prev_position) < 0)
	    running.duration = 0;
	  else
	    {
	      running.duration -= (position - prev_position);	
	    }
	}

      if (position == new_process->insertion_time)
	{	  
	  if ((int)running.duration - (int)(new_process->insertion_time - position) <= 0)
	    {
	      running.duration = 0;
	      position += 1;
	      break;
	    }
	  
	  else
	    {
	      running.duration -= new_process->insertion_time - position;
	      position += 1;
	      conflict_priority->push_back(running.priority);
	      break;
	    }
	}

      if (running.duration != 0 && queue.front().duration < running.duration)
	{
	  running.insertion_time = position;
	  queue.push_back(running);
	  running = queue.front();
	  queue.erase(queue.begin());
	}
      
      else if (running.duration == 0)
	{
	  if (queue.empty() && proc_list.empty())	    
	    break;	    
	  
	  else if (!queue.empty())
	    {
	      running = queue.front();
	      queue.erase(queue.begin());
	    }
	}
      
      prev_position = position;

      if (proc_list.empty())
	position += running.duration;
      
      else if (running.duration != 0 && position + running.duration < proc_list.front().insertion_time)
	position += running.duration;

      else
	position = proc_list.front().insertion_time;

      if (position > new_process->insertion_time)
	position = new_process->insertion_time;
    }
  
  if (running.duration != 0)
    conflict_priority->push_back(running.priority);

  if (!queue.empty())
    {
      printf("ps pre in queue %ld\n", queue.size());
    }
  
  for (process_t proc : queue)
    conflict_priority->push_back(proc.priority); 
}
