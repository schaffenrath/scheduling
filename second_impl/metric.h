#ifndef METRICS_H
#define METRICS_H

#include "queue.h"
#include "process.h"

typedef struct proc_metric
{
  unsigned short id;
  float turnaround_time;
  float response_time;
  short response_flag;
  float wait_time;
  proc_metric *next;
} proc_metric_t;

typedef struct metric
{
  proc_metric_t *proc_metric_ptr;
  float avg_turnaround_time;
  float avg_response_time;
  float avg_wait_time;
} metric_t;

metric_t calc_metrics(queue_pos_t *queue);
void print_metrics(metric_t metric);
void clear_metrics(proc_metric_t *metric_ptr);
#endif
