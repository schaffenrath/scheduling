#ifndef QUEUE_H
#define QUEUE_H

#define ID queue->tail->running_process->process.id
#define INSERTION queue->tail->running_process->process.insertion_time
#define REMAINING queue->tail->running_process->process.remaining_time
#define RUNNING queue->tail->running_process
#define POSITION queue->tail->position
#define PTR queue->tail->proc_list_pointer
#define PTR_INSERTION queue->tail->proc_list_pointer->process.insertion_time;
#define PREV_RUNNING queue->tail->prev->running_process
#define PREV_PTR queue->tail->prev->proc_list_pointer
#define PREV_POSITION queue->tail->prev->position
#define PREV_REMAINING queue->tail->prev->running_process->process.remaining_time
#define LIST_PROC queue->tail->processes->process

#include "process.h"

typedef struct queue_t
{
  unsigned short position;
  unsigned short total_tickets;
  process_list_t *running_process;
  process_list_t *proc_list_pointer;
  process_list_t *processes;
  struct queue_t *next;
  struct queue_t *prev;
} queue_t;

typedef struct queue_pos_t
{
  queue_t *head;
  queue_t *tail;  
} queue_pos_t;

void queue_append (queue_pos_t **queue, process_list_t *process_list);
queue_t *queue_copy_last_node (queue_t *queue);
void queue_clear (queue_pos_t *queue);

#endif
