#ifndef PROCESS_H
#define PROCESS_H

typedef struct process
{
  unsigned short id;
  unsigned short insertion_time;
  unsigned short duration;
  unsigned short remaining_time;
  unsigned short priority;
  unsigned short tickets;
  unsigned short quantum_usage;
} process_t;

typedef struct process_list
{
  process_t process;
  struct process_list *next;
  struct process_list *prev;
} process_list_t;

process_list_t *proc_find (process_list_t *head, const unsigned short proc_id);
void proc_insert (process_list_t **head, const process_t *new_process);
void proc_append (process_list_t **head, const process_t *new_process);
process_list_t * proc_copy (process_list_t *head, process_list_t **old_to_new_pointer);
void proc_remove_front (process_list_t **head);
void proc_remove_pointer (process_list_t **head, process_list_t *pointer_to_remove);
void proc_remove_process (process_list_t **head, const unsigned short process_id);
void proc_print (process_list_t *head);
void proc_clear (process_list_t *head);

#endif
