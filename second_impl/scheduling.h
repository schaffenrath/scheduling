#ifndef SCHEDULING_H
#define SCHEDULING_H

#include "schedule_functions.h"
#include "process.h"
#include "queue.h"

#define NON_PREEMTIVE 0
#define PREEMTIVE 1
#define FCFS 0
#define SJF 1
#define SRTF 2
#define RR 3
#define PS 4
#define LS 5

queue_t *scheduling (short scheduling_type, process_list_t *proc_list_head);
queue_t *scheduling (short scheduling_type, process_list_t *proc_list_head, unsigned short additional_argument);

#endif
