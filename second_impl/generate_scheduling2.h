#ifndef GENERATE_SCHEDULING2_H
#define GENERATE_SCHEDULING2_H

#include "process.h"

void check_sjf(std::vector<process_t> proc_list, process_t *new_process,
	       std::vector<unsigned short> *conflict_remaining);

void check_srtf(std::vector<process_t> proc_list, process_t *new_process,
		std::vector<unsigned short> *conflict_remaining);

void check_rr(std::vector<process_t> proc_list, unsigned short quantum, 
	      std::vector<unsigned short> *conflict_remaining);

void check_ps_non(std::vector<process_t> proc_list, process_t *new_process,
		  std::vector<unsigned short> *conflict_priority);

void check_ps_pre(std::vector<process_t> proc_list, process_t *new_process,
		  std::vector<unsigned short> *conflict_priority); 
#endif
