#include <iostream>
#include <vector>
#include <algorithm>
#include "process.h"
#include "generate_scheduling.h"


/*
 *	This file contains the functions to count the conflicts for FCFS, SJF, SRTF, RR and PS. These functions are called in generate_processes.cpp
 *  to verify the randomly generated attributes of the processes. 
 * 
 *  The scheduling functions in this file differ from the functions in scheduling.cpp, because mainly the process list is here a vector, as where
 *  in scheduling.cpp it is a double linked list. The functions here also don't provide information on the order of execution and no additional data
 *  is stored.
 */


bool is_earlier(process_t x, process_t y)
{
  //if insertionTime is equal, the process with lower id is preferred
  if (x.insertion_time == y.insertion_time)
    return false;
  return x.insertion_time < y.insertion_time;
}

bool is_shorter(process_t x, process_t y)
{
  //if duration and insertion are equal, order via id
  if (x.remaining_time == y.remaining_time)
    {
      if (x.insertion_time == y.insertion_time)
	{
	  return false;
	}
      else
	{
	  return x.insertion_time < y.insertion_time;
	}
    }
  return x.remaining_time < y.remaining_time;
}

bool has_higher_priority(process_t x, process_t y)
{
  //if priority and insertion are equal, order via id
  if (x.priority == y.priority)
    {
      if (x.insertion_time == y.insertion_time)
	{
	  return false;
	}
      else
	{
	  return x.insertion_time < y.insertion_time;
	}
    }
  return x.priority < y.priority;
}

bool is_earlier_and_shorter(process_t x, process_t y)
{
  //if insertion and duration are equal, order via id
  if (x.insertion_time == y.insertion_time)
    {
      if (x.remaining_time == y.remaining_time)
	{
	  return false;
	}
      else
	{
	  return x.remaining_time < y.remaining_time;
	}
    }
  
  return x.insertion_time < y.insertion_time;
}

bool is_earlier_and_higher_priority(process_t x, process_t y)
{
  //if insertion and priority are equal, order via id
  if (x.insertion_time == y.insertion_time)
    {
      if (x.priority == y.priority)
	{
	  return false;
	}
      else
	{
	  return x.priority < y.priority;
	}
    }
  return x.insertion_time < y.insertion_time;
}


unsigned short check_fcfs(process_list_t *head, process_t *new_process)
{
  std::vector<process_t> fcfs_process_list;

  process_list_t *iterator = head;
  while (iterator != NULL)
    {
      fcfs_process_list.push_back(iterator->process);
      iterator = iterator->next;
    }
  fcfs_process_list.push_back(*new_process);
  
  std::sort(fcfs_process_list.begin(), fcfs_process_list.end(), is_earlier);

  unsigned short conflicts = 0;
  process_t tmp_save; 
  for (process_t proc : fcfs_process_list)
    {
      if (proc.id != fcfs_process_list.front().id)
	if (tmp_save.insertion_time == proc.insertion_time)	  
	    conflicts++;
      
      tmp_save = proc;
    }
  return conflicts;
}

unsigned short check_sjf(std::vector<process_t> ordered_process_list, process_t new_process)
{
  ordered_process_list.push_back(new_process);
  std::sort(ordered_process_list.begin(), ordered_process_list.end(), is_earlier_and_shorter);
  
  unsigned short current_position = 0, current_index = 0, conflicts = 0;
  process_t *current_process, *prev_process;
  bool potential_processes_found = false;
  std::vector<process_t> compare_process_list;
  std::vector<process_t> sjf_process_list;
  
  while (!ordered_process_list.empty())
    {
      potential_processes_found = false;
      compare_process_list.clear();
      current_index=0;
      current_process = &ordered_process_list.front();
      prev_process=NULL;

      if (current_process->insertion_time > current_position)
	current_position = current_process->insertion_time;
      
      while (!potential_processes_found)
      	{	  
      	  current_process = &ordered_process_list.at(current_index);
      	  if (prev_process != NULL)
      	    {
	      // when old processes are done and new processes are in the future, than current_process is next
      	      if (current_process->insertion_time > prev_process->insertion_time &&
		  current_process->insertion_time > current_position)
      		{
		  potential_processes_found = true;
      		  break;
      		}
      	    }
	  //compare_process_list contains all possible processes concerning insertion 
      	  compare_process_list.push_back(*current_process);
	  
      	  if ((current_index+1) == (int)ordered_process_list.size())	    
	    potential_processes_found = true;	 	    
      	  current_index++;
	  prev_process = current_process;
       	}
       
      std::sort(compare_process_list.begin(), compare_process_list.end(), is_shorter);

      // if 2 processes have same remaining time, an alternative solution exists
      if (compare_process_list.size() > 1)
	{
	  process_t tmp_save;
	  for (process_t candidate : compare_process_list)
	    {
	      if (candidate.remaining_time == tmp_save.remaining_time)               
                    conflicts++;                
	      tmp_save = candidate;
	    }
	}
      
      current_position += compare_process_list.front().remaining_time;
      sjf_process_list.push_back(compare_process_list.front());

      //removing used processes from initial list
      for (int i=0; i<(int)ordered_process_list.size(); i++)
	{
	  if (ordered_process_list.at(i).id == compare_process_list.front().id)
	    {	      		
	      ordered_process_list.erase(ordered_process_list.begin()+i);			      
	    }
	}  	  
    }
  
  return conflicts;
}


unsigned short check_srtf(std::vector<process_t> ordered_process_list, process_t new_process)
{
  ordered_process_list.push_back(new_process);
  std::sort(ordered_process_list.begin(), ordered_process_list.end(), is_earlier_and_shorter);

  unsigned short process_start_position=0, current_position=0, current_index=0, index_iterator=0;
  unsigned short remaining_duration = 0, conflicts = 0;
  process_t *current_process=NULL;
  bool found_shorter = false, found_next = false;;
  std::vector<process_t> compare_process_list;
  std::vector<process_t> srtf_process_list;

  while (!ordered_process_list.empty())
    {     
      if (found_shorter)	
	found_shorter = false;
      else
	current_index = 0;
      
      found_next = false;
      current_process = &ordered_process_list.at(current_index);
      
      if (current_process->insertion_time > current_position)
	current_position = current_process->insertion_time;	

      process_start_position = current_position;
      index_iterator = (current_index + 1);

      // if list is empty or insertion of the next process is after the current process is finished
      if (index_iterator >= (int)ordered_process_list.size() ||
	  (current_position + current_process->remaining_time) < ordered_process_list.at(index_iterator).insertion_time)
	{
	  current_process->remaining_time = 0;
	  compare_process_list.push_back(*current_process);	    
	  ordered_process_list.erase(ordered_process_list.begin()+current_index);
	}
      
      else
	{
	  if (ordered_process_list.at(index_iterator).insertion_time <= current_position)
	    {
	      int shortest_process_index = -1;
	      process_t tmp_save;
	      // iterate through processes inserted before current position
	      while (index_iterator < (int)ordered_process_list.size() &&
		     ordered_process_list.at(index_iterator).insertion_time <= current_position)
		{
		  // if process with shorter remaining time was found, save it
		  if (current_process->remaining_time > ordered_process_list.at(index_iterator).remaining_time)
		    {
		      shortest_process_index = index_iterator;
		      tmp_save = ordered_process_list.at(index_iterator);
		    }

		  for (size_t j=index_iterator+1; j < ordered_process_list.size() &&
			 ordered_process_list.at(j).insertion_time <= current_position; j++)
		    if (tmp_save.remaining_time == ordered_process_list.at(j).remaining_time)
		      conflicts++;
		  
		  index_iterator++;
		}


	      
	      if (shortest_process_index != -1)
		{
		  compare_process_list.push_back(ordered_process_list.at(shortest_process_index));
		  ordered_process_list.erase(ordered_process_list.begin() + shortest_process_index);
		  found_next = true;
		}
	    }
	  
	  if (!found_next)
	    {
	      //search for shorter process until end of list or until the insertion of next process is after the current process finished
	      while (index_iterator < (int)ordered_process_list.size() && !found_shorter &&
		     (current_position + ordered_process_list.at(current_index).remaining_time) > ordered_process_list.at(index_iterator).insertion_time)
		{
		  //termination time of current_process - insertion time of next process
		  remaining_duration = (process_start_position + current_process->remaining_time) - ordered_process_list.at(index_iterator).insertion_time;       
		  if (remaining_duration > ordered_process_list.at(index_iterator).remaining_time)
		    {
		      // every process with same remaining time results in alternative solution
		      for (int i=index_iterator; i < (int)ordered_process_list.size() &&
			     current_position + ordered_process_list.at(index_iterator).remaining_time > ordered_process_list.at(i).insertion_time; i++)
			{
			  for (int j=i+1; j < (int)ordered_process_list.size() &&
				 current_position + ordered_process_list.at(index_iterator).remaining_time > ordered_process_list.at(j).insertion_time; j++)
			    {			
			      if (ordered_process_list.at(i).remaining_time == ordered_process_list.at(j).remaining_time)
				conflicts++;
			    }
			}
		      
		      //split interrupted process
		      process_t split_process = ordered_process_list.at(current_index);
		      split_process.remaining_time = remaining_duration;

		      //add interrupted process to new vector and remove it from the ordered_process_list
		      split_process.insertion_time = current_position;
		      current_process->remaining_time = remaining_duration;
		      compare_process_list.push_back(*current_process);
		      ordered_process_list.insert(ordered_process_list.begin()+index_iterator+1, split_process);
		 
		      ordered_process_list.erase(ordered_process_list.begin());
		
		      found_shorter = true;
		      //index_iterator - 1, because one element was removed earlier
		      current_index = (index_iterator-1);
		    }
		  index_iterator++;
		}

	      //if no shorter process was found, continue with current process
	      if (!found_shorter)
		{
		  compare_process_list.push_back(*current_process);
		  current_position = (process_start_position + current_process->remaining_time);
		  ordered_process_list.erase(ordered_process_list.begin()+current_index);
		}
	    }
	}
    }

  return conflicts;
}




unsigned short check_rr(std::vector<process_t> ordered_process_list, process_t new_process, const unsigned short quantum)
{
  ordered_process_list.push_back(new_process);
  std::sort(ordered_process_list.begin(), ordered_process_list.end(), is_earlier);

  unsigned short current_position = 0, index_iterator = 0, conflicts = 0;
  process_t *current_process;
  std::vector<process_t> rr_process_list;

  while (!ordered_process_list.empty())    
    {
      index_iterator = 0;
      current_process = &ordered_process_list.front();
      for (int i=1; i < (int)ordered_process_list.size() && ordered_process_list.at(i).insertion_time == current_process->insertion_time; i++)
	conflicts++;
      
      if (current_process->insertion_time > current_position)
	current_position = current_process->insertion_time;
      
      if (current_process->remaining_time > quantum)
	{
	  current_position += quantum;	  
	  process_t split_process = *current_process;
	  split_process.insertion_time = current_position;
	  split_process.remaining_time = split_process.remaining_time - quantum;	 
	  rr_process_list.push_back(*current_process);

	  //search position in list, where process insertion is after current position
	  while (index_iterator < (int)ordered_process_list.size() &&
		 ordered_process_list.at(index_iterator).insertion_time <= current_position)	    
	    index_iterator++;

	  //insert split process in the previously searched position
	  if (index_iterator > 0 || index_iterator <= ((int)ordered_process_list.size()-1)) 
	    ordered_process_list.insert(ordered_process_list.begin() + index_iterator, split_process);
	  else
	    ordered_process_list.push_back(split_process);
	  
	  ordered_process_list.erase(ordered_process_list.begin());
	}
      
      else
	{
	  rr_process_list.push_back(*current_process);
	  current_position += quantum;
	  ordered_process_list.erase(ordered_process_list.begin());
	}
    }

  return conflicts;
}


unsigned short check_ps_non(std::vector<process_t> ordered_process_list, process_t new_process, process_t *conflict_process)
{  
  ordered_process_list.push_back(new_process);  
  std::sort(ordered_process_list.begin(), ordered_process_list.end(), is_earlier_and_higher_priority);
  
  unsigned short current_position=0, current_index=0, conflicts = 0;
  process_t *current_process, *prev_process;
  bool potential_processes_found = false;
  std::vector<process_t> compare_process_list;
  std::vector<process_t> ps_non_process_list;
  
  while (!ordered_process_list.empty())
    {
      potential_processes_found = false;
      compare_process_list.clear();
      current_index=0;
      current_process = &ordered_process_list.front();
      prev_process=NULL;

      if (current_process->insertion_time > current_position)
	current_position = current_process->insertion_time;
      
      while (!potential_processes_found)
      	{	  
      	  current_process = &ordered_process_list.at(current_index);
      	  if (prev_process != NULL)
      	    {
	      // when old processes are done and new processes are in the future, than current_process is next
      	      if (current_process->insertion_time > prev_process->insertion_time &&
		  current_process->insertion_time > current_position)
      		{
		  potential_processes_found = true;
      		  break;
      		}
      	    }
	  
      	  compare_process_list.push_back(*current_process);	  
      	  if ((current_index+1) == (int)ordered_process_list.size())	    
	    potential_processes_found = true;	 	    
      	  current_index++;
	  prev_process = current_process;
       	}
       
      std::sort(compare_process_list.begin(), compare_process_list.end(), has_higher_priority);

      
      // if 2 processes have same priority, an alternative solution exists
      if (compare_process_list.size() > 1)
	{
	  process_t tmp_save;
	  for (process_t candidate : compare_process_list)
	    {
	      if (candidate.id != compare_process_list.front().id)
		{
		  if (candidate.priority == tmp_save.priority &&
		      candidate.insertion_time == tmp_save.insertion_time){
		    *conflict_process = candidate;
		    conflicts++;
		  }
		}
	      tmp_save = candidate;
	    }
	}
      
      
      current_position += compare_process_list.front().remaining_time;
      ps_non_process_list.push_back(compare_process_list.front());

      //removing used processes from initial list
      for (int i=0; i<(int)ordered_process_list.size(); i++)
	{
	  if (ordered_process_list.at(i).id == compare_process_list.front().id)
	    {
	      ordered_process_list.erase(ordered_process_list.begin()+i);		
	      break;
	    }
	}  	  
    }

  return conflicts;
}



unsigned short check_ps_pre(std::vector<process_t> ordered_process_list, process_t new_process, process_t *conflict_process)
{
  ordered_process_list.push_back(new_process);
  std::sort(ordered_process_list.begin(), ordered_process_list.end(), is_earlier_and_higher_priority);

  
  unsigned short process_start_position=0, current_position=0, current_index=0, index_iterator=0;
  unsigned short remaining_duration = 0, conflicts = 0;
  process_t *current_process=NULL;
  bool found_shorter = false, found_next = false;;
  std::vector<process_t> compare_process_list;
  std::vector<process_t> ps_pre_process_list;
  
  while (!ordered_process_list.empty())
    {     
      if (found_shorter)	
	found_shorter = false;
      else
	current_index = 0;
      
      found_next = false;
      current_process = &ordered_process_list.at(current_index);
      
      if (current_process->insertion_time > current_position)
	current_position = current_process->insertion_time;	

      process_start_position = current_position;
      index_iterator = (current_index + 1);

      //if end of list or insertion of next process is after current process is finished
      if (index_iterator >= (int)ordered_process_list.size() ||
	  (current_position + current_process->remaining_time) < ordered_process_list.at(index_iterator).insertion_time)
	{
	  current_process->remaining_time = 0;
	  compare_process_list.push_back(*current_process);	    

	  if ((int)ordered_process_list.size() > 1)
	    ordered_process_list.erase(ordered_process_list.begin()+current_index);
	  else
	    ordered_process_list.clear();
	}
      
      else
	{
	  if (ordered_process_list.at(index_iterator).insertion_time <= current_position)
	    {	 
	      int highest_priority_process_index = -1;
	      process_t *priority_ptr = NULL;
	      while (index_iterator < (int)ordered_process_list.size() &&
		     ordered_process_list.at(index_iterator).insertion_time <= current_position)
		{
		  if (current_process->priority > ordered_process_list.at(index_iterator).priority)
		    {
		      highest_priority_process_index = index_iterator;
		      priority_ptr = &ordered_process_list.at(index_iterator);
		    }

		  else if (priority_ptr != NULL &&
			   priority_ptr->priority == ordered_process_list.at(index_iterator).priority)
		    {
		      conflicts++;
		      *conflict_process = ordered_process_list.at(index_iterator);
		    }
		    
		  index_iterator++;
		}
	      
	      if (highest_priority_process_index != -1)
		{
		  compare_process_list.push_back(ordered_process_list.at(highest_priority_process_index));
		  ordered_process_list.erase(ordered_process_list.begin() + highest_priority_process_index);
		  found_next = true;
		}
	    }
	  
	  if (!found_next)
	    {
	      while (index_iterator < (int)ordered_process_list.size() && !found_shorter &&
		     (current_position + ordered_process_list.at(current_index).remaining_time) > ordered_process_list.at(index_iterator).insertion_time)
		{
		  //termination-time from current_process - insertion-time from next process
		  remaining_duration = (process_start_position + current_process->remaining_time) - ordered_process_list.at(index_iterator).insertion_time;       
		  if (current_process->priority > ordered_process_list.at(index_iterator).priority)
		    {

		      for (int i=index_iterator+1; i<(int)ordered_process_list.size() &&
			     ordered_process_list.at(i).priority == ordered_process_list.at(index_iterator).priority; i++)
			{
			  conflicts++;
			  *conflict_process = ordered_process_list.at(index_iterator);
			}
		      //split interrupted process
		      process_t split_process = ordered_process_list.at(current_index);
		      split_process.remaining_time = remaining_duration;

		      //add interrupted process to new vector and remove it from the ordered_process_list
		      split_process.insertion_time = current_position;
		      current_process->remaining_time = remaining_duration;
		      compare_process_list.push_back(*current_process);
		      ordered_process_list.insert(ordered_process_list.begin()+index_iterator+1, split_process);
		 
		      ordered_process_list.erase(ordered_process_list.begin());
		
		      found_shorter = true;
		      //index_iterator - 1, because one element was removed earlier
		      current_index = (index_iterator-1);
		    }
		  index_iterator++;
		}

	      if (!found_shorter)
		{
		  compare_process_list.push_back(*current_process);
		  current_position = (process_start_position + current_process->remaining_time);
		  ordered_process_list.erase(ordered_process_list.begin()+current_index);

		}
	    }
	}
    }
  return conflicts;
}
