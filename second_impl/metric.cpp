#include "metric.h"
#include <cstdio>
#include <cstdlib>
#include <cfloat>


/*
 *  This file contains functions to calculate the metrics: turnaround time, response time, wait time for every process and the 
 *  average for all processes in the list. 
 */


proc_metric_t *metric_find_proc(proc_metric_t **head, unsigned short search_id)
{
  proc_metric_t *it = *head;
  while (it != NULL)
    {
      if (it->id == search_id)
	return it;
      it = it->next;     
    }

  proc_metric_t *new_node = (proc_metric_t*) malloc(sizeof(proc_metric_t));
  new_node->id = search_id;
  new_node->turnaround_time = 0.0;
  new_node->response_time = FLT_MAX;
  new_node->response_flag = 0;
  new_node->wait_time = 0.0;
  new_node->next = NULL;

  if (*head == NULL)
    *head = new_node;

  else
    {
      it = *head;
      while (it->next != NULL)
	it = it->next;
      it->next = new_node;
    }
  return new_node;
}

void calc_proc_metrics(proc_metric_t **head, queue_pos_t *queue)
{
  queue_t *queue_it = queue->head;
  while (queue_it != NULL)
    {
      if (queue_it->processes != NULL)
	{
	  process_list_t *proc_it = queue_it->processes;
	  while (proc_it != NULL)
	    {
	      proc_metric_t *metric = metric_find_proc(head, proc_it->process.id);
	      if (!metric->response_flag)
		{
		  if (proc_it->process.id == queue_it->running_process->process.id)
		    {
		      // when response_time hasn't been set, than process is running instantly
		      if (metric->response_time == FLT_MAX)
			metric->response_time = 0;
		      else
			metric->response_time = queue_it->position - metric->response_time;
		      metric->response_flag = 1;
		    }
		  // if response_time hasn't been set, change it to first apperance in queue to
		  // easaly calculate the response time 
		  else
		    {
		      if (metric->response_time == FLT_MAX)
			{
			  metric->response_time = queue_it->position;
			}		      
		    }
		}

	      // increase wait time
	      if (proc_it->process.id != queue_it->running_process->process.id)
		metric->wait_time += (queue_it->next->position - queue_it->position);

	      // increase turnaround time
	      metric->turnaround_time += (queue_it->next->position - queue_it->position);
	      
	      proc_it = proc_it->next;
	    }
	}
      queue_it = queue_it->next;
    }
}

metric_t calc_avg_metrics(proc_metric_t *metric_head)
{
  metric_t new_metrics;
  unsigned short proc_counter = 0;
  float avg_turnaround = 0.0, avg_response = 0.0, avg_wait = 0.0;
  new_metrics.proc_metric_ptr = metric_head;

  proc_metric_t *it = metric_head;
  while (it != NULL)
    {
      proc_counter++;
      avg_turnaround += it->turnaround_time;
      avg_response += it->response_time;
      avg_wait += it->wait_time;
      it = it->next;
    }
  new_metrics.avg_turnaround_time = avg_turnaround / proc_counter;
  new_metrics.avg_response_time = avg_response / proc_counter;
  new_metrics.avg_wait_time = avg_wait / proc_counter;
  
  return new_metrics;
}

metric_t calc_metrics(queue_pos_t *queue)
{
  proc_metric_t *metric_head = NULL;
  calc_proc_metrics(&metric_head, queue);  
  return calc_avg_metrics(metric_head);
}

void clear_metrics(proc_metric_t *metric_ptr)
{
  proc_metric_t *it = metric_ptr;
  proc_metric_t *tmp = NULL;
  while (it != NULL)
    {
      tmp = it->next;
      free(it);
      it = tmp;
    }
  metric_ptr = NULL;
}

void print_metrics(metric_t metric)
{
  proc_metric_t *it = metric.proc_metric_ptr;
  while (it != NULL)
    {
      printf("id: %hu, turn: %f, resp: %f, wait: %f\n", it->id,
	     it->turnaround_time, it->response_time, it->wait_time);
      it = it->next;
    }
  printf("\n");
  printf("AVG turnaround time: %f\n", metric.avg_turnaround_time);
  printf("AVG response time: %f\n", metric.avg_response_time);
  printf("AVG wait time: %f\n\n", metric.avg_wait_time);
  
}

