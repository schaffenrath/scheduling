#include <ctime>
#include <cmath>
#include <climits>
#include <iostream>
#include <vector>
#include <random>
#include "generate_processes.h"
#include "generate_scheduling.h"

using namespace std;


/*
 *	This file contains the functions to create random processes with a specific amount of conflicts. The attributes for the processes are 
 *  generated here and the verify-functions from generate_scheduling.cpp are called. 
 */

random_device rd;
  mt19937 gen(rd());

bool check_conflicts(bool *remaining_found, bool should_conflict, unsigned int conflicts, unsigned int conflict_counter, unsigned short *tries)
{
  if (!should_conflict && conflicts > 0)
	{
	  *remaining_found = false;
	  *tries += 1;
	  return true;
	}
      else if ((should_conflict && conflicts == 0) || (should_conflict && conflicts > conflict_counter+1))
	{
	  *remaining_found = false;
	  *tries += 1;
	  return true;
	}
   return false;
}

unsigned short generate_unique_id(std::vector<process_t> *proc_list, unsigned short max)
{
  uniform_real_distribution<double> rando(0.0,(double) max);

  if (proc_list->empty())
    return (unsigned short) rando(gen);

  bool unique_id_found = false;
  unsigned short potential_id = 0;

  while (!unique_id_found)
    {
      potential_id = (unsigned short)rando(rd);
      unique_id_found = true;
      for (process_t proc : *proc_list)
	{
	  if (proc.id == potential_id)
	    {
	      unique_id_found = false;
	      break;
	    }
	}
    }
  return potential_id;
}

unsigned short generate_insertion_time(std::vector<process_t> *proc_list, bool should_conflict,
				        unsigned short max)
{
  uniform_real_distribution<double> rando(0.0,(double) max);

  if (proc_list->empty())
    return (unsigned short) rando(gen);

  bool insertion_found = false;
  unsigned short potential_insertion = 0;

  while (!insertion_found)
    {
      potential_insertion = (unsigned short) rando(rd);
      insertion_found = true;
      for (size_t i=0; i < proc_list->size() && insertion_found; i++)       
	{	 	 
	  if (proc_list->at(i).insertion_time == potential_insertion)
	    {
	      if (!should_conflict)		
		insertion_found = false;
	      else
		break;
	    }
	  
	  else if (should_conflict && i == proc_list->size()-1)
	    insertion_found = false;	  
	}
    }
  
  return potential_insertion;
}

unsigned short generate_remaining_time(std::vector<process_t> *proc_list,
				       process_t *new_proc,
				       bool should_conflict,
				       unsigned short max,
				       unsigned short conflict_counter,
				       unsigned short quantum)
{
  
  uniform_real_distribution<double> rando(1.0,(double) max);

  // max remaining time depends on the quantum to avoid conflicts
  uniform_real_distribution<double> rando_restricted(1.0,(double) floor(max/quantum));

  if (proc_list->empty()) {
    unsigned short return_value = (unsigned short) rando(gen);
    
    return return_value;   
  }

  bool remaining_found = false;
  unsigned short conflicts = 0, tries = 0, changed_insertion = 0;
  
  while (!remaining_found)
    {
      if (!should_conflict)
	new_proc->remaining_time = (unsigned short) rando(gen);

      else
	new_proc->remaining_time = (unsigned short) rando_restricted(gen);
      
      remaining_found = true;

      // if no valid remaining time was found, change the insertion time
      if (tries >= max*4)
	{
	  new_proc->insertion_time = generate_insertion_time(proc_list, should_conflict, max);
	  changed_insertion++;

	  // if no valid remaining time with different insertion times was found
	  // signal that to start over again
	  if (changed_insertion >= 100)
	    return 0;
	}
      
      conflicts = check_sjf(*proc_list, *new_proc);
      if(check_conflicts(&remaining_found, should_conflict, conflicts, conflict_counter, &tries))
        {       
          //printf("sjf conflict\n");
            continue;
        }
      
      conflicts = check_srtf(*proc_list, *new_proc);;
      if (check_conflicts(&remaining_found, should_conflict, conflicts, conflict_counter, &tries))
        {
         //printf("srtf conflict\n");
	continue;
        }
      
      conflicts = check_rr(*proc_list, *new_proc, quantum);
      if (check_conflicts(&remaining_found, should_conflict, conflicts, conflict_counter, &tries))
        {
         //printf("rr conflict\n");
	continue;
        }

    }
  return new_proc->remaining_time;
}

unsigned short generate_priority(std::vector<process_t> *proc_list, process_t *new_proc,
				 bool should_conflict, unsigned short conflict_counter)
{
  uniform_real_distribution<double> rando(1.0, 10.0);

  if (proc_list->empty())    
    return  (unsigned short) rando(gen);;

  bool priority_found = false;
  unsigned short conflicts = 0, tries = 0;
  process_t conflict_proc;
  
  while (!priority_found)
    {
      new_proc->priority = rando(rd);
      priority_found = true;

      if (tries >= 100)
	return 0;

      conflicts = check_ps_non(*proc_list, *new_proc, &conflict_proc);
      if (!should_conflict && conflicts > 0)
	{
	  priority_found = false;
	  tries++;
	  if (tries >= 20)
	    {	      
	      for (size_t i=0; i<proc_list->size(); i++)
		{
		  if (proc_list->at(i).id == conflict_proc.id)		    
		    proc_list->at(i).priority = rando(rd);		    
		}
	      if (tries >= 100)
		return 0;	      
	    }
	  continue;
	}

      else if ((should_conflict && conflicts == 0) || (should_conflict && conflicts > conflict_counter+1))
	{
	  priority_found = false;
	  tries++;
	}
      
      conflicts = check_ps_pre(*proc_list, *new_proc, &conflict_proc);
      if (!should_conflict && conflicts > 0)
	{
	  priority_found = false;
	  tries++;
	  if (tries >= 20)
	    {
	      for (size_t i=0; i<proc_list->size(); i++)
		{
		  if (proc_list->at(i).id == conflict_proc.id)
		    proc_list->at(i).priority = rando(rd);
		}
	      if (tries >= 100)
		return 0;       	
	    }
	  continue;
	}

      else if ((should_conflict && conflicts == 0) || (should_conflict && conflicts > conflict_counter+1))
	{
	  priority_found = false;
	  tries++;
	}
    }
  return new_proc->priority;
}


void generate_procs(process_list_t *head, unsigned short amount, unsigned short max, unsigned short alt_solutions)
{
  bool done = false, should_conflict = false;
  unsigned short created = 0, tries = 0, conflict_counter = 0;
  std::vector<process_t> new_proc_list;
  
  while (!done)
    {
      should_conflict = false;
      if (alt_solutions > 0 && created >= amount - alt_solutions)
	should_conflict = true;
      
      process_t new_proc;
      new_proc.id = generate_unique_id(&new_proc_list, max);
      new_proc.insertion_time = generate_insertion_time(&new_proc_list, should_conflict, max);
      new_proc.remaining_time = generate_remaining_time(&new_proc_list, &new_proc,
							should_conflict, max, conflict_counter, 4);

      // if no valid remaining time was found, start over again
      if (new_proc.remaining_time == 0)
	{
	  new_proc_list.clear();
	  created = 0;
	  tries++;
	  if (tries >= 100)
	    {
	      printf("no valid remaining time was found!\n");
	     	head = NULL;
				return;
	    }
	  continue;
	}
      
      new_proc.priority = generate_priority(&new_proc_list, &new_proc, should_conflict, conflict_counter);

      if (new_proc.priority == 0)
	{
	  new_proc_list.clear();
	  created = 0;
	  tries++;
	  if (tries >= 100)
	    {
	      printf("no valid priority was found!\n");
	      head = NULL;
				return;
	    }
	  continue;
	}
      
      new_proc.tickets = 0;
      new_proc.quantum_usage = 0;

      new_proc_list.push_back(new_proc);
      created++;

      if (should_conflict)
	conflict_counter++;
      
      if (created >= amount)
	done = true;

    }
  
  for (process_t proc : new_proc_list)
    {
      std::cout<<"\nid: "<<proc.id<<"\n";
      std::cout<<"insertion: "<<proc.insertion_time<<"\n";
      std::cout<<"remaining: "<<proc.remaining_time<<"\n";
      std::cout<<"priority: "<<proc.priority<<"\n";
			proc_insert(&head, &proc);
    }
}
