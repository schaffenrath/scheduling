#ifndef GENERATE_SCHEDULING_H
#define GENERATE_SCHEDULING_H

#include "process.h"

unsigned short check_fcfs(process_list_t *head, process_t *new_process);
unsigned short check_sjf(std::vector<process_t> ordered_process_list, process_t new_process);
unsigned short check_srtf(std::vector<process_t> ordered_process_list, process_t new_process);
unsigned short check_rr(std::vector<process_t> ordered_process_list, process_t new_process, const unsigned short  quantum);
unsigned short check_ps_non(std::vector<process_t> ordered_process_list, process_t new_process, process_t *conflict_process);
unsigned short check_ps_pre(std::vector<process_t> ordered_process_list, process_t new_process, process_t *conflict_process);
#endif
