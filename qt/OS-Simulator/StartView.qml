import QtQuick 2.0
import QtQuick.Controls 2.2

Page {
    id: proc
    width: main.width
    height: main.height
    title: qsTr("Process Scheduling")

    Text {
        id: mainHeading
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: 40
        text: "Betriebssystem Services"
        color: "#828282"
        font.family: "Ubuntu"
        font.bold: true
        font.pixelSize: parent.width/18
        font.underline: true
    }

    Row {
        id: row2
        anchors.centerIn: parent
        spacing: 10

        StartSelectButton {
            id: schedulingButton
            width: proc.width/3 - 20
            height: proc.width/3 - 20
            source: "assets/scheduling-icon.png"
            text: "Scheduling"
            onClicked: stackView.push("SchedulingInput.qml")
        }

        StartSelectButton {
            id: deadlockButton
            width: proc.width/3 - 20
            height: proc.width/3 - 20
            source: "assets/deadlock-icon.png"
            text: "Deadlock"
        }

        StartSelectButton {
            id: memoryButton
            width: proc.width/3 - 20
            height: proc.width/3 - 20
            source: "assets/memory-icon.png"
            text: "Speicher"
        }

    }

    Rectangle {
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: 10
        width: 100
        height: 30
        border.color: "#ff4747"
        border.width: 1.5
        radius: 1

        MouseArea {
            id: quitArea
            anchors.fill: parent
            cursorShape: Qt.PointingHandCursor
            onClicked: Qt.quit()
        }

        Text {
            id: quitLabel
            anchors.centerIn: parent
            text: "Beenden"
            color: "#ff4747"
            font.family: "Ubuntu"
            font.pixelSize: 12
            font.bold: true
            font.letterSpacing: 1.1
        }
    }

}
