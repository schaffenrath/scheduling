import QtQuick 2.0
import QtQuick.Controls 2.3

ApplicationWindow {
    id: main
    visible: true
    width: 640
    height: 480
    title: qsTr("OS Simulator")

    property alias stackView: stackView
    property alias main : main


    StackView {
        id: stackView
        initialItem: "StartView.qml"
        anchors.fill: parent
    }
}
