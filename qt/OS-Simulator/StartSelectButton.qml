import QtQuick 2.0

Rectangle {
    id: button
    border.color: "#828282"
    border.width: 5
    radius: 1

    property alias source: icon.source
    property alias text: description.text
    signal clicked

    Column {
        id: row
        anchors.centerIn: parent
        anchors.margins: 100
        spacing: 1

        Image {
            id: icon
            width: button.width - 60
            anchors.horizontalCenter: parent.horizontalCenter
            fillMode: Image.PreserveAspectFit
            clip: true

        }

        Text {
            id: description
            anchors.horizontalCenter: parent.horizontalCenter
            color: "#828282"
            font.family: "Ubuntu"
            font.pixelSize: parent.height/7
        }
    }

    MouseArea {
        id: buttonMouseArea
        anchors.fill: parent
        cursorShape: Qt.PointingHandCursor
        onClicked: button.clicked()
    }
}

