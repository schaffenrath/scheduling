import QtQuick 2.0
import QtQuick.Controls 2.2

Page {
    id: proc2

    Image {
        id: backButton
        x: 10; y: 10
        source: "assets/arrow-back.png"
        width: 40
        fillMode: Image.PreserveAspectFit
        clip: true

        MouseArea {
            id: backMouseArea
            anchors.fill: parent
            cursorShape: Qt.PointingHandCursor
            onClicked: stackView.pop("SchedulingInput.qml")
        }
    }

    Text {
        id: header
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: 20
        color: "#828282"
        font.family: "Ubuntu"
        font.pixelSize: parent.width/24
        font.bold: true
        font.underline: true
        text: "Scheduling"
    }

    SwipeView {
        id: swipeView
        currentIndex: 0
        width: parent.width
        height: parent.height - 80
        anchors.bottom: parent.bottom

        Item {
            id: inputPage

            Rectangle {
                id: typeSelectionBorder
                border.color: "#828282"
                border.width: 2
                width: parent.width - 60
                height: parent.height/5
                anchors.horizontalCenter: parent.horizontalCenter


            }
        }

        Item {
            id: secondPage


        }
    }

    PageIndicator {
        id: indicator

        count: swipeView.count
        currentIndex: swipeView.currentIndex

        anchors.bottom: swipeView.bottom
        anchors.horizontalCenter: parent.horizontalCenter
    }



}
